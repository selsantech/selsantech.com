<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8">
	<meta http-equiv="content-language" content="en">
	<meta http-equiv="cache-control" content="no-cache">
	<title>SelsanTech</title>
	<link type="text/css" rel="stylesheet" href="css/style.css"></link>
	<link href="css/style9.css" type="text/css" rel="stylesheet"></link>
	<link href="css/demo.css" type="text/css" rel="stylesheet"></link>
	<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Terminal+Dosis"></link>
	<style>
	
	A
	{
		outline: 0;
	}
	
	A:link    {color:blue; text-decoration:none; outline: none; -moz-outline-style: none;}
	A:visited {color:blue; text-decoration:none; outline: none; -moz-outline-style: none;}
	A:hover   {color:blue; text-decoration:none; outline: none; -moz-outline-style: none;}
	A:active  {color:black; text-decoration:none; outline: none; -moz-outline-style: none;}
	
	HTML, BODY
	{
		background-image: url("image/bg.png");
		background-color: white;
	}

	HEADER
	{
		height: 250px;
	}
	
	FOOTER
	{
		border-width: 1px 0px 0px 0px;
		border-color: #888;
		border-style: solid;
		width: auto;
		height: auto;
		background-color: #aaaaff;
		text-align: center;
		padding-bottom: 40px;
	}
	
	FOOTER H3
	{
		font-size: 20px;
		font-weight: bold;
	}
	
	FOOTER H4, FOOTER LI
	{
		font-size: 25px;
	}
	
	.innersection
	{
		min-height: 800px;
		margin-top: 40px;
		margin-bottom: 40px;
	}

	.innersection H3
	{
		font-size: 60px;
		color: black;
		text-align: left;

		background-color: transparent;
		padding-top: 12px;
		margin-bottom: 40px;
	}

	.innersection H4
	{
		text-align: justify;
		margin: 20px;
		font-size: 30px;
	}

	.innersection LI
	{
		text-align: justify;
		margin: 35px;
		font-size: 30px;
	}

	.fixedmenu
	{
		position: fixed;
		bottom: 40px;
		right: 30px;
		display: block;
	}

	.menuitem
	{
		cursor: pointer;
		padding: 10px;
		height: 30px;
		width: 30px;
	}

	.logo
	{
		margin: 120px 30px 30px 30px;
		padding: 20px;
		text-align: center;
		vertical-align: middle;
		background-color: white;
		border: 10px solid #f6f6f6;
		-webkit-box-shadow: 1px 1px 2px rgba(0,0,0,0.2);
		-moz-box-shadow: 1px 1px 2px rgba(0,0,0,0.2);
		box-shadow: 1px 1px 2px rgba(0,0,0,0.2);
		-webkit-border-radius: 125px;
		-moz-border-radius: 125px;
		border-radius: 125px;
		width: 940px;
	}

	.inline-logo
	{
		/*margin: 120px 30px 30px 30px;
		padding: 20px;
		background-color: white;
		border: 10px solid #f6f6f6;
		-webkit-box-shadow: 1px 1px 2px rgba(0,0,0,0.2);
		-moz-box-shadow: 1px 1px 2px rgba(0,0,0,0.2);
		box-shadow: 1px 1px 2px rgba(0,0,0,0.2);
		-webkit-border-radius: 125px;
		-moz-border-radius: 125px;
		border-radius: 125px; */
		display: inline-block;
		text-align: left;
		vertical-align: middle;
		position: relative;
		/*left: 200px; */

	}


	#product .item
	{
		width: 940px;
		background: url("image/line.png") no-repeat scroll 0px -18px transparent;
		display: inline-block;
		margin-bottom: 30px;
	}

	#product IMG
	{
		float: left;
		width: 200px;
		padding: 20px;
	}
	
	.box
	{
		text-align: left;
		display: inline-block;
		margin: 40px;
		vertical-align: top;
	}

	</style>
</head>
<body>

<div class="container">
	<header>

		<center>
		<div class="logo">
			<img src="./image/selsantech.png" />
		</div>
		</center>
		<div class="fixedmenu">
			<a href="#"><div class="menuitem"><span class="sh-icon">:</span></div></a>
			<a href="#us"><div class="menuitem"><span class="sh-icon">U</span></div></a>
			<a href="#expertise"><div class="menuitem"><span class="sh-icon">C</span></div></a>
			<a href="#product"><div class="menuitem"><span class="sh-icon">D</span></div></a>
			<a href="#purpose"><div class="menuitem"><span class="sh-icon">S</span></div></a>
			<a href="#partners"><div class="menuitem"><span class="sh-icon">K</span></div></a>
		</div>
		<img style="position:fixed; bottom: 10px; left: 10px;" src="http://api.qrserver.com/v1/create-qr-code/?data=http://goo.gl/fL9Rxc&size=60x60"/>
		
	</header>

	<div class="content">

		<ul class="ca-menu">
			<li><a href="#us">
				<span class="ca-icon">U</span>
				<div class="ca-content">
					<h2 class="ca-main">Who we are</h2>
					<h3 class="ca-sub">High qualified team</h3>
				</div>
			</a></li>
			<li><a href="#expertise">
				<span class="ca-icon">C</span>
				<div class="ca-content">
					<h2 class="ca-main">Expertise</h2>
					<h3 class="ca-sub">Applied science and experience</h3>
				</div>
			</a></li>
			<li><a href="#product">
				<span class="ca-icon">D</span>
				<div class="ca-content">
					<h2 class="ca-main">Products</h2>
					<h3 class="ca-sub">Advanced use of technology</h3>
				</div>
			</a></li>
			<li><a href="#purpose">
				<span class="ca-icon">S</span>
				<div class="ca-content">
					<h2 class="ca-main">Purposes</h2>
					<h3 class="ca-sub">Engineering in action</h3>
				</div>
			</a></li>
		</ul>

		<div style="height:500px;"></div>

		<main>

		<section class="innersection" id="us">
			<h3><span class="tl-icon">U</span>Who we are</h3>
			<h4>We are a small enterprise created about one year ago. Our focus is to be a provider of innovation and high technology for industry and end consumers. </h4>
			<h4>Our team features:</h4>
			<ul>
			<li type=disc> Researchers with PhD degree and strong international academic background (Max Plank Institute, Georgia Tech, Unversity of São Paulo)
			<li type=disc> Computer Scientists and Engineers capable of developing custom hardware/software for a handful of applications
			<li type=disc> Compliance with requirements to obtain government funding (in Brazil, at least...)
			</ul>			

		</section>

		<section class="innersection" id="expertise">
			<h3><span class="tl-icon">C</span>Our expertise</h3>
			<h4>Besides our academic track, we've been involved in the development of commercial solutions related to our expertise, such as </h4>
			<ul>
			<li type=disc> <strong>Information Security: </strong>solutions for secure online banking
			<li type=disc> <strong>Computer vision: </strong>system for automated part inspection and tracking
			<li type=disc> <strong>Embedded Systems: </strong> hardware and software for Home Automation
			<li type=disc> <strong>Lerning from Big Data: </strong>machine learning system for tax category classification of products (Brazil has a rich ecosystem of such categories...)
			</ul>			
		</section>


		<section class="innersection" id="product">
		    <h3><span class="tl-icon">D</span>Products</h3>
		    <div class="item">
			<img src="image/catsearch_logo.png" style="margin-right: 20px;"/>
			<h4>Tool for automated categorical classification from natural language description and other information. Uses knowledge acquired by learning from <strong>Big Data</strong></h4>
		    </div>
		    <div class="item">
			<img src="image/saidafacil.png" style="margin-right: 20px;"/>
			<h4>A tool to organize the logistic of school exit time by telling when student parents are coming to pick them up, preventing large waiting times and improving the traffic around the school. Works by tracking parents using smartphones through their GPS and GPRS network-based location service</h4>
		    </div>
		    <div class="item">
			<img src="image/zigpoint.png" style="margin-right: 20px;  padding-bottom:100px;"/>
			<h4>The gateway for devices to enter the M2M and IoT world. It involves all the hardware and software needed to enable wireless device communication, control and tracking from anywhere in the world. The product is in the final stage of development</h4>
		    </div>
		    <div class="item">
			<img src="image/zigtrack.png" style="margin-right: 20px;"/>
			<h4>ZigTrack is an automated inventory system that tracks from the most expensive equipment to the cheapest materials using cutting-edge technology, gaining productivity and avoiding theft and waste. Received FAPESP grant number 12/51397-5</h4>
		    </div>
		</section>

		<section class="innersection" id="purpose">
			<h3><span class="tl-icon">S</span>Purposes</h3>
			<h4>What we can offer: </h4>
			<ul>
			<li type="disc"> We want to give to every machine the possibility to be part of the <strong>Smart World</strong> </li>
			<li type="disc"> With our technology it is possible to estimate in real time <strong>the location</strong> of a piece of equipament and <strong>to control</strong> it remotely through a mesh (redundant) network</li>
			<li type="disc">We have a modular solution that fits mains and battery powered devices</li>
			<h4>Lab prototype modules:</h4>
			<img src="./image/layers.jpg" class="modulephoto">
			<h4>Wireless power control assembly:</h4>
			<img src="./image/assembled.jpg" class="modulephoto">
			<h4>Simple box for communication and control:</h4>
			<img src="./image/box.jpg" class="modulephoto">
			<h4>Application demonstration (July, 2013)</h4>
			<video src="./demo.mov" controls="controls" width="800" height="600">
				Your browser does not support the video tag.
			</video>
			<h4>Our technology can be used to build a M2M network giving low cost support for <strong>Smart Cities</strong>, which can bring many benefits to the society such as:</h4>
			<ul>
			<li type=disc>Real time monitoring of public services: illumination, public transportation, efficient use of utilities, prediction and notification of disasters, etc.
			<li type=disc>Opening <strong>new markets</strong> for <strong>new companies</strong> based on the data generated by such network, creating <strong>new jobs</strong>, for instance: services for small vehicle, pet and people tracking around the city, deliverance of media on demand, intelligent operation of traffic lights and signs, and so on
			<li type=disc>Improving security, integrating private alarm systems to a broader network, allowing them to share information
			</ul>			
			<h4>We and our partners are also qualified to design and deliver solutions based on mathematical approaches to process the data generated by monitoring devices in this highly integrated network</h4>
		</section>

		<section class="innersection" id="partners">
			<h3><span class="tl-icon">K</span>Partner: 
			<a href="http://www.minimo.net.br"><div class="inline-logo">
				<img style="width:250px; padding-bottom:4px;background-color:white; border-bottom:solid gray 2px" src="image/minimo_logo.jpg">
			</div></a> </h3>
			
	
			<h4>Minimo acts to solve business needs which requires advanced mathematics for complex problem solving.</h4>
			<h4>The company conducts applied research, develops computational tools and web applications to solve specific problems which require advanced applied mathematics, such as:
			
			<div style="line-height:75%"><ul>
			<li type='disc'>Modeling and solving of optimization problems</li>
			<li type='disc'>Modeling and simulation of dynamic systems</li>
			<li type='disc'>Signal processing and control systems design</li>
			<li type='disc'>Time series forecasting, econometric analysis, demand forecasting</li>
			<li type='disc'>Statistical modeling</li>
			<li type='disc'>Topological analysis of complex networks</li>
			</ul></div>
		</section>

		</main>

	</div>

	<footer>
		<div class="content">
			<div class="box">
				<h3>Links</h3>
				<br/>
				<ul>
					<li><a href="http://www.selsantech.com/">CatSearch</a></li>
					<li><a href="http://www.saidafacil.com/">Saída Fácil</a></li>
					<li><a href="http://www.zigpoint.com/">ZigPoint</a></li>
					<li><a href="http://www.zigtrack.com/">ZigTrack</a></li>
				</ul>
			</div>
			<div class="box" style="width: 450px;">
				<h3>Contact</h3>
				<img style="float: right; margin-left: 20px;" src="http://api.qrserver.com/v1/create-qr-code/?data=BEGIN%3AVCARD%0AVERSION%3A2.1%0AFN%3AHumberto%20Sandmann%0AN%3ASandmann%3BHumberto%0ATITLE%3APhD.%0AEMAIL%3BWORK%3BINTERNET%3Ahumberto%40selsantech.com%0AURL%3Awww.selsantech.com%0AADR%3A%3B%3BR.%20Eng%C2%B0%20Jo%C3%A3o%20Monteiro%20da%20Gama%2C%20162%3BS%C3%A3o%20Paulo%3B%3B04144-120%3BBrasil%0AORG%3ASelsanTech%0AEND%3AVCARD%0A&size=130x130" />
				<br/>
				<h4>Humberto Sandmann</h4>
				<h4><a href="mailto:humberto@selsantech.com">humberto@selsantech.com</a></h4>
				<h4>+55 (11) 981 199 071</h4>
				<br/>
				<h3>Selsan Tecnologia Industrial e Comercial LTDA</h3>
				<h3>Rua Eng° João Monteiro da Gama, 162</h3>
				<h3>04144-120 São Paulo, SP - Brazil</h3>
			</div>
		</div>
			
		<b>Selsan Tecnologia Industrial e Comercial LTDA</b>
		<br/>Copyright 2014 SelsanTech® - All rights are reserved
		
	</footer>
</div>
</body>
</html>
