<?php

ini_set('display_errors', 0);

	header('Content-type: application/json');
	header('Pragma: public');
	header('Cache-control: private');
	header('Expires: -1');
	
	$output = array();

	try
	{
		
		include '../_userlib.php';
		
		$frnm = $_REQUEST['firstname'];
		$mdnm = $_REQUEST['middlename'];
		$lsnm = $_REQUEST['lastname'];
		$btrhdt = $_REQUEST['birthdate'];
		$email = $_REQUEST['email'];
		$pass  = $_REQUEST['pass'];
		$photo = $_REQUEST['photo'];
		
		$photo = $photo != null && strlen($photo) ? base64_decode($photo) : null;
		
		if (userIdByEmail($email) == null)
		{
			userInsert($frnm, $mdnm, $lsnm, $btrhdt, $email, $pass, $photo);
		}
		else
		{
			throw new Exception('This email has already been registrated');
		}
		
	}
	catch (Exception $e)
	{
		array_push($output, array('error' => array (array ('message' => $e->getMessage()))));
	}
	echo json_encode($output, true);
	
?>
