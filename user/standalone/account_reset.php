<?php

ini_set('display_errors', 0);

	header('Content-type: application/json');
	header('Pragma: public');
	header('Cache-control: private');
	header('Expires: -1');
	
	$output = array();

	try
	{
		
		include '../_userlib.php';
		
		$email = $_REQUEST['email'];
		
		if (userIdByEmail($email) != null)
		{
			resetAccount($email);
		}
		else
		{
			throw new Exception('There is not this email at our database');
		}

	}
	catch (Exception $e)
	{
		array_push($output, array('error' => array (array ('message' => $e->getMessage()))));
	}
	echo json_encode($output, true);
	
?>
