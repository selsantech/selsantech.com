<?php

	header('Content-type: application/json');
	header('Pragma: public');
	header('Cache-control: private');
	header('Expires: -1');
	
	$output = array();

	try
	{

		include '../_userlib.php';
		
		$email = $_REQUEST['email'];
		$pass  = $_REQUEST['pass'];
		
		array_push($output, array('user' => login($email, $pass, false)));
		
	}
	catch (Exception $e)
	{
	//	array_push($output, array('error' => array (array ('code' => 0, 'message' => $e->getMessage()))));
		array_push($output, array('error' => array (array ('message' => $e->getMessage()))));
	}
	
	echo json_encode($output, true);
	
?>
