<?php

	try
	{
		include './_userlib.php';
		
		if (isset($_REQUEST['submit']))
		{
			
			$email = $_REQUEST['email'];
			$pass  = $_REQUEST['pass'];
			login($email, $pass);
			
			if (isset($_REQUEST['sucess']) && strlen($_REQUEST['sucess']) > 0)
			{
				header("Location: ../" . $_REQUEST['sucess']);
			}
			throw new Exception('A página de sucesso não foi configurada');
			
		}
	}
	catch (Exception $e)
	{
		$error = $e->getMessage();
		if (isset($_REQUEST['fail']) && strlen($_REQUEST['fail']) > 0)
		{
			header("Location: ../" . $_REQUEST['fail']);
		}
		echo $error;
	}

?>
