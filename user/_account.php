	<script src='./user/script/account.js'></script>
	<?php
		$link = loginDBLink();
		$res = userInfoById($link, $_SESSION['usrid']);
		if ($res && $res->num_rows == 1)
		{
			$row = $res->fetch_row();
			$frnm = $row[1];
			$mdnm = $row[2];
			$lsnm = $row[3];
			$brthdt = $row[4];
			if ($brthdt != null && strlen($brthdt) > 0)
			{
				$date = DateTime::createFromFormat('Y-m-d', $brthdt);
				$brthdt = $date->format('d/m/Y');
			}
			$emailcurrent = $row[5];
			$email = "";
			$pht = $row[6];
		}
		if (isset($_REQUEST['submit']))
		{
			$frnm = $_REQUEST['frnm'];
			$mdnm = $_REQUEST['mdnm'];
			$lsnm = $_REQUEST['lsnm'];
			$brthdt = $_REQUEST['brthdt'];
			$email = $_REQUEST['email'];
		}
		$link->close();
	?>
	
	<div class="account">
		<h3>Conta</h3>
		
		<form id="accountform" action="" method="post" onsubmit="return validate();">
		
		<div class="figure">
			<img id="photopreview" src="data:image/jpeg;base64,<?php echo base64_encode($pht); ?>" />
			<input type="file" name="photo" id="photo" style="display: none;" onchange="preview_photo();" />
			<input type="hidden" name="hasphoto" id="hasphoto" value="0" />
			<input type="button" id="btnselectphoto" value="Selecionar" style="width: 70px;" onclick="select_photo();" />
			<input type="button" id="btnclearphoto" value="Apagar" style="width: 70px;" onclick="clear_photo();" />
		</div>

		<table>
			<tr>
				<td class="label" span="mandatory">Primeiro nome</td>
				<td>
					<input type="text" value="<?php echo $frnm; ?>" name="frnm" id="frnm" maxlength="50" onblur="validate_frnm();" />
					<span class="error" id="errfrnm"></span>
				</td>
			</tr>
			<tr>
				<td class="label">Nome do meio</td>
				<td>
					<input type="text" value="<?php echo $mdnm; ?>" name="mdnm" id="mdnm" maxlength="50"/>
					<span class="error" id="errmdnm"></span>
				</td>
			</tr>
			<tr>
				<td class="label" span="mandatory">Sobrenome</td>
				<td>
					<input type="text" value="<?php echo $lsnm; ?>" name="lsnm" id="lsnm" maxlength="50" onblur="validate_lsnm();" />
					<span class="error" id="errlsnm"></span>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td class="label">Data de nascimento</td>
				<td>
					<input type="text" value="<?php echo $brthdt; ?>" name="brthdt" id="date" />
					<span id="hint">(dd/mm/aaaa)</span>
				</td>
			</tr>
			
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td class="label">Email atual</td>
				<td>
					<input type="text" value="<?php echo $emailcurrent; ?>" class="email" disabled />
				</td>
			</tr>
			<tr>
				<td name="emailarea" id="emailarea" class="label">Novo email</td>
				<td name="emailarea" id="emailarea">
					<input type="text" value="<?php echo $email; ?>" class="email" name="email" id="email" onblur="validate_email();" />
					<span class="error" id="erremail"></span>
				</td>
			</tr>
			<tr>
				<td name="emailarea" id="emailarea" class="label">Redigite o novo email</td>
				<td name="emailarea" id="emailarea">
					<input type="text" value="" class="email" name="emailagain" id="emailagain" onblur="validate_email();" />
					<span class="error" id="erremailagain"></span>
				</td>
			</tr>
			
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td class="label">Senha atual</td>
				<td>
					<input type="password" value="" name="passcurrent" id="passcurrent" onblur="validate_password();" />
					<span class="error" id="errpasscurrent"></span>
				</td>
			</tr>
			<tr>
				<td name="passarea" id="passarea" class="label">Nova senha</td>
				<td name="passarea" id="passarea">
					<input type="password" value="" name="pass" id="pass" onblur="validate_password();" />
					<span class="error" id="errpass"></span>
				</td>
			</tr>
			<tr>
				<td name="passarea" id="passarea" class="label">Redigite a nova senha</td>
				<td name="passarea" id="passarea">
					<input type="password" value="" name="passagain" id="passagain" onblur="validate_password();" />
					<span class="error" id="errpassagain"></span>
				</td>
			</tr>
			
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td colspan="2" style="text-align: right;">
					<input type="button" value="Alterar email" id="btnchangeemail" onclick="javascript:changeEmail();" />
					<input type="button" value="Alterar senha" id="btnchangepass" onclick="javascript:changePass();" />
					<input name="submit" type="submit" value="Salvar" id="btnsave" />
				</td>
			</tr>
		</table>
		</form>
		
	</div>
