<?php

	define("EVENT_INACTIVE", 0);
	define("EVENT_ACTIVE", 1);
	
	define("EVENT_TYPE_REGISTER_USER", 1);
	define("EVENT_TYPE_ACTIVE_USER", 2);
	define("EVENT_TYPE_RESET_PASSWORD", 3);
	define("EVENT_TYPE_UPDATE_DATA", 4);
	define("EVENT_TYPE_UPDATE_EMAIL", 5);
	define("EVENT_TYPE_UPDATE_PASSWORD", 6);
	define("EVENT_TYPE_UPDATE_EMAIL_AND_PASSWORD", 7);
	
	function loginDBLink()
	{
		// $mysql_host = "box890.bluehost.com";
		$mysql_host = "localhost";
		$mysql_database = "jhsimove_auth";
		$mysql_user = "jhsimove_login";
		$mysql_password = "login@SST";

		$link = new mysqli($mysql_host, $mysql_user, $mysql_password, $mysql_database);
		if ($link->connect_errno)
		{
			throw new Exception('Falhou ao conectar ao banco de dados. Por favor, tente novamente mais tarde! ' . $link->connect_error);
		}
		
		$link->set_charset("utf8");
		$link->query("SET TIME_ZONE = '-3:00';");
		return $link;
	}

	function logout()
	{
		session_start();
		session_unset();
		session_destroy();
	}
	
	function isLogged()
	{
		return (isset($_SESSION['sessionid']) && isset($_SESSION['usrid']));
	}
	
	function checkSession()
	{
		if(!isLogged())
		{
			logout();
			header('Location: /');
			return false;
		}
		return true;
	}

	function login($email, $pass, $createSession)
	{
		logout();
		if (strlen($email) == 0 || strlen($pass) == 0)
		{
			throw new Exception('Usuário e/ou Senha esta(ão) em branco');
		}
		
		$link = loginDBLink();
		
		$email = $link->real_escape_string($email);
		$pass  = $link->real_escape_string($pass);
		$hash  = calcHash($pass);
		
		$sql = "SELECT usr_id, usr_email, usr_frnm, usr_mdnm, usr_lsnm, usr_brthdt, usr_pht
	              FROM user
                 WHERE usr_email = '{$email}' AND usr_sha256 = '{$hash}';";
		
		$res = $link->query($sql);

		if ($res && $res->num_rows == 1)
		{
			if (isset($createSession) && $createSession) {
				$row = $res->fetch_row();
				session_start();
				// session_regenerate_id();
				$_SESSION['usrid'] = $row[0];
				$_SESSION['usrnm'] = $row[2] . ' ' . $row[4	];
				$_SESSION['sessionid'] = true;
			}
			
			$info = resultToArray($res);
				
			$res->close();
			$link->close();
			return $info;
		}
		else
		{
			throw new Exception('Usuário e senha não conferem');
		}
	}
	
	function validatePassword($usrid, $pass) {
		$link = loginDBLink();
		
		$pass  = $link->real_escape_string($pass);
		$hash  = calcHash($pass);
		
		$sql = "SELECT usr_id "
			 . "  FROM user "
			 . " WHERE usr_id = {$usrid} AND usr_sha256 = '{$hash}';";
		
		$res = $link->query($sql);
		if ($res && $res->num_rows == 1) {
			$res->close();
			$link->close();
			return true;
		}
		$res->close();
		$link->close();
		return false;
	}
		
	function calcHash($word)
	{
		return hash('sha256', $word);
	}
	
	function userInfoById($link, $usrid)
	{
		$usrid = $link->real_escape_string($usrid);
		$sql = "SELECT usr_id, usr_frnm, usr_mdnm, usr_lsnm, usr_brthdt, usr_email, usr_pht
				  FROM user
				 WHERE usr_id = '{$usrid}';";
		return $link->query($sql);
	}
	
	function userIdByEmail($email)
	{
		$link = loginDBLink();
		$email = $link->real_escape_string($email);
		$sql = "SELECT usr_id FROM user WHERE usr_email = ?;";
		$stmt = $link->prepare($sql);
		$stmt->bind_param("s", $email);
		$stmt->execute();
		$stmt->bind_result($usrid);
		if ($stmt->fetch()) {
			$link->close();
			return $usrid;
		}
		$link->close();
		return null;
	}
	
	function userInfoXMLById($usrid)
	{
		$link = loginDBLink();
		$res = userInfoById($link, $usrid);
		$xml = new SimpleXMLElement('<entries/>');
		while ($res && $res->num_rows > 0 && $row = $res->fetch_row())
		{
			$entry = $xml->addChild('userinfo');
			$entry->addChild("usrid", $row[0]);
			$entry->addChild("usrnm", $row[1]);
			$entry->addChild("usremail", $row[2]);
		}
		$content = $xml->asXML();
		
		if ($res)
		{
			$res->close();
		}
		$link->close();
		return $content;
	}
	
	function userInsert($frnm, $mdnm, $lsnm, $btrhdt, $email, $pass, $photo)
	{
		try
		{
			$ip = $_SERVER['REMOTE_ADDR'];
			$link   = loginDBLink();
			$frnm   = $link->real_escape_string($frnm);
			$mdnm   = $link->real_escape_string($mdnm);
			$lsnm   = $link->real_escape_string($lsnm);
			$btrhdt = $link->real_escape_string($btrhdt);
			$email  = $link->real_escape_string($email);
			$pass   = $link->real_escape_string($pass);
			$photo  = strlen($photo) > 0 ? addslashes($link->real_escape_string($photo)) : null;
			$hash   = calcHash($pass);
			
			$mdnm = strlen($mdnm) == 0 ? null : $mdnm;
			$btrhdt = strlen($btrhdt) == 0 ? null : $btrhdt;
			
			echo $photo;
			
			$sql = "INSERT INTO user (usr_frnm, usr_mdnm, usr_lsnm, usr_email, usr_sha256, usr_brthdt, usr_pht) "
				 . "VALUES (?, ?, ?, ?, ?, ?, ?)";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("sssssss", $frnm, $mdnm, $lsnm, $email, $hash, $btrhdt, $photo);
			$stmt->execute();
			$id = $link->insert_id;
			$stmt->close();
			
			$sql = "INSERT INTO event (usr_id, evt_addr, evt_act, ett_id) "
				 . "VALUES (?, ?, " . EVENT_ACTIVE . ", " . EVENT_TYPE_REGISTER_USER . ")";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("is", $id, $ip);
			$stmt->execute();
			$stmt->close();
			
			$link->close();
			return $id;
		}
		catch (Exception $e)
		{
			throw new Exception("Usuário não inserido. " . $e->getMessage());
		}
	}
	
	function userUpdate($usrid, $frnm, $mdnm, $lsnm, $btrhdt, $email, $pass, $photo)
	{
		try
		{
			$ip = $_SERVER['REMOTE_ADDR'];
			$event = EVENT_TYPE_UPDATE_DATA;
			$link   = loginDBLink();
			$frnm   = $link->real_escape_string($frnm);
			$mdnm   = $link->real_escape_string($mdnm);
			$lsnm   = $link->real_escape_string($lsnm);
			$btrhdt = $link->real_escape_string($btrhdt);
			$email  = $link->real_escape_string($email);
			$pass   = $link->real_escape_string($pass);
			$photo  = strlen($photo) > 0 ? addslashes($link->real_escape_string($photo)) : null;
				
			$mdnm = strlen($mdnm) == 0 ? null : $mdnm;
			$btrhdt = strlen($btrhdt) == 0 ? null : $btrhdt;
			
			$sql = "UPDATE user SET usr_frnm=?, usr_mdnm=?, usr_lsnm=?, usr_brthdt=?"; // TODO photo
			if (strlen($email) > 0)
			{
				$sql .= ", usr_email='{$email}'";
				$event = EVENT_TYPE_UPDATE_EMAIL;
			}
			if (strlen($pass) > 0)
			{
				$hash = calcHash($pass);
				$sql .= ", usr_sha256='{$hash}'";
				$event = $event != EVENT_TYPE_UPDATE_DATA ? EVENT_TYPE_UPDATE_EMAIL_AND_PASSWORD : EVENT_TYPE_UPDATE_PASSWORD;
			}
			$sql .= " WHERE usr_id=?";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("ssssi", $frnm, $mdnm, $lsnm, $btrhdt, $usrid);
			$stmt->execute();
			$stmt->close();

			$sql = "INSERT INTO event (usr_id, evt_addr, evt_act, ett_id) "
				 . "VALUES (?, ?, " . EVENT_ACTIVE . ", ?)";
			$stmt = $link->prepare($sql);
			$stmt->bind_param("isi", $usrid, $ip, $event);
			$stmt->execute();
			$stmt->close();
				
			$link->close();
		}
		catch (Exception $e)
		{
			throw new Exception("Usuário não atualizado. " . $e->getMessage());
		}
	}

	function resultToArray($result)
	{
		$rows = array();
		while ($row = $result->fetch_assoc()) $rows[] = $row;
		return $rows;
	}
	
	function resetAccount($email)
	{
		$usrid = userIdByEmail($email);
		$ip = $_SERVER['REMOTE_ADDR'];
		$code = strtoupper(bin2hex(openssl_random_pseudo_bytes(8)));
		registerReset($ip, $usrid, $code);
		
		$to = $email;
		$subject = "Reset Selsantech Account";
		$message = "
				<html>
					<head><title>Reset Selsantech Account</title></head>
					<body>
						<p>Dear User,</p>
						<br />
						<p>A request to reset password was done through of IP {$ip}.</p>
						<p>To confirm the reset, please click on the below link and inform the following code:</p>
						</br>
						<p>link: www.selsantech.com/reset?email={$email}&code={$code}
						<br />code: {$code}
						<br />(expire on 24 hours)</p>
						<br />
						<p>We appreciate your contact,
						<br />Selsantech Team</p>
					</body>
				</html>
			";
		$from = "webmaster@selsantech.com";
		
		$headers = array();
		$headers[] = "MIME-Version: 1.0";
		$headers[] = "Content-type: text/html; charset=utf-8";
		$headers[] = "To: {$to}";
		$headers[] = "From: Selsan Account Center <{$from}>";
		$headers[] = "Subject: {$subject}";
		$headers[] = "X-Mailer: PHP/".phpversion();
		
		mail($to, $subject, $message, implode("\r\n", $headers));
	}
	
	function registerReset($ip, $usrid, $code)
	{
		try
		{
			$link = loginDBLink();
			
			$sql = "UPDATE event SET evt_act=" . EVENT_INACTIVE . " WHERE usr_id=? AND eet_id=" . EVENT_TYPE_RESET_PASSWORD;
			$stmt = $link->prepare($sql);
			$stmt->bind_param('i', $usrid);
			$stmt->execute();
			$stmt->close();
			
			$sql = "INSERT INTO event (usr_id, evt_addr, evt_act, ett_id, evt_code) VALUES (?, ?, "
					. EVENT_ACTIVE . ", " . EVENT_TYPE_RESET_PASSWORD . ", ?)";
			$stmt = $link->prepare($sql);
			$stmt->bind_param('iss', $usrid, $ip, $code);
			$stmt->execute();
			$rows = $stmt->affected_rows;
			$stmt->close();
			$link->close();
			return $rows;
		}
		catch (Exception $e)
		{
			throw new Exception("Reset was not registered. " . $e->getMessage());
		}
	}
		
?>
