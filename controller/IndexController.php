<?php

include_once './controller/MainController.php';

class IndexController
{
	/**
	 * Ação que deverá ser executada quando
	 * nenhuma outra for especificada, do mesmo jeito que o
	 * arquivo index.html ou index.php é executado quando nenhum
	 * é referenciado
	 */
	public function indexAction()
	{
		(new MainController)->homeAction();
	}
}

?>