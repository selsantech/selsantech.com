<?php

class MainController
{
	
	public static function posfixLanguage()
	{

		if (!isset($_SESSION)) session_start();
		
		$lang = isset($_SESSION['language'])
			  ? $_SESSION['language']
			  : substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
		
		return $lang == 'pt' ? 'pt/' : 'en/';
		
	}
	
	public function languageAction()
	{
		if (isset($_REQUEST['id']))
		{
			if (!isset($_SESSION)) session_start();
			$_SESSION['language'] = $_REQUEST['id'];
			 header("Location: http://".$_SERVER['SERVER_NAME'].dirname(dirname($_SERVER['REQUEST_URI']))) ;
			//$this->homeAction();
		}
	}

	public function homeAction()
	{
		$view = new View('./view/' . MainController::posfixLanguage() . 'consulting.php');
		$view->setParams(array ("page" => "consulting"));
		$view->showContents();
	}

	public function productAction()
	{
		$view = new View('./view/' . MainController::posfixLanguage() . 'product.php');
		$view->setParams(array ("page" => "product"));
		$view->showContents();
	}

	public function consultingAction()
	{
		$view = new View('./view/' . MainController::posfixLanguage() . 'consulting.php');
		$view->setParams(array ("page" => "consulting"));
		$view->showContents();
	}

	public function casesAction()
	{
		$view = new View('./view/' . MainController::posfixLanguage() . 'cases.php');
		$view->setParams(array ("page" => "cases"));
		$view->showContents();
	}
	
	public function solutionAction()
	{
		$view = new View('./view/' . MainController::posfixLanguage() . 'solutions.php');
		$view->setParams(array ("page" => "solution"));
		$view->showContents();
	}

	public function automationAction()
	{
		$view = new View('./view/' . MainController::posfixLanguage() . 'automation.php');
		$view->setParams(array ("page" => "automation"));
		$view->showContents();
	}
	
	public function partnerAction()
	{
		$view = new View('./view/' . MainController::posfixLanguage() . 'partners.php');
		$view->setParams(array ("page" => "partner"));
		$view->showContents();
	}
	
	public function contactAction()
	{
		$view = new View('./view/' . MainController::posfixLanguage() . 'contact.php');
		$view->setParams(array ("page" => "contact"));
		$view->showContents();
	}
	
	public function aboutAction()
	{
		$view = new View('./view/' . MainController::posfixLanguage() . 'about.php');
		$view->setParams(array ("page" => "about"));
		$view->showContents();
	}
	
	public function zigbeeAction()
	{
		$view = new View('./view/' . MainController::posfixLanguage() . 'product_zigbee.php');
		$view->setParams(array ("page" => "solution"));
		$view->showContents();
	}

	public function residentialAction()
	{
		$view = new View('./view/' . MainController::posfixLanguage() . 'automation_residential.php');
		$view->setParams(array ("page" => "automation"));
		$view->showContents();
	}
	
	public function buildingAction()
	{
		$view = new View('./view/' . MainController::posfixLanguage() . 'automation_building.php');
		$view->setParams(array ("page" => "automation"));
		$view->showContents();
	}
	
	public function smartcitiesAction()
	{
		$view = new View('./view/' . MainController::posfixLanguage() . 'automation_smartcities.php');
		$view->setParams(array ("page" => "automation"));
		$view->showContents();
	}
	
	/**
	 * Ação a ser executada ao receber dados vindos
	 * de um formulário
	 */
	public function formAction()
	{
		$name = $_POST['Name'];
		$email = $_POST['Email'];
		$from = $name . ' <' . $email . '>';
	
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text; charset=iso-8859-1' . "\r\n";
		$headers .= 'To: Contato <contato@selsantech.com>' . "\r\n";
		$headers .= 'From: ' . $from . "\r\n";
		$headers .= 'Reply-To: ' . $from . "\r\n";
		$headers .= 'X-Mailer: PHP/' . phpversion();
	
		$data = print_r($_POST, true);
	
		//Send email to us
		if (mail("contato@selsantech.com", "[selsan] Contato via site" , $data, $headers)) {
			die();
		}
		header('HTTP/1.1 500 Internal Server Booboo');
		header('Content-Type: application/json; charset=UTF-8');
		die(json_encode(array('message' => 'Mensagem não enviada', 'code' => 0)));
	}
	
}

?>
