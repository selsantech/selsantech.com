function slideText(id)
{
    var text = document.getElementById(id + "-text");
    var pane = document.getElementById(id + "-pane");
    text.style.animation="slider 1s forwards";
    text.style.WebkitAnimation="slider 1s forwards";
    pane.style.animation="slider 1s forwards";
    pane.style.WebkitAnimation="slider 1s forwards";

}

function unslideText(id)
{
    var text = document.getElementById(id + "-text");
    var pane = document.getElementById(id + "-pane");
    text.style.animation="unslider 1s forwards";
    text.style.WebkitAnimation="unslider 1s forwards";
    pane.style.animation="unslider 1s";
    pane.style.WebkitAnimation="unslider 1s forwards";
}

