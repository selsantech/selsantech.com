
$(function() {
	
	$("#appshow").click(function() {
		$("#appmenu").slideToggle();
	});
	
	
	$("#contact-button").click(function () {

		$("#reply_text").html("Enviando mensagem...");

		$.ajax({
			type: "POST",
			url: "form",
			data: $("#contact-form").serialize()
		}).done(function( msg ) {
			$("#reply_text").html("Obrigado, sua mensagem foi enviada com sucesso" + msg);
		}).fail(function( jqXHR, textStatus ) {
			$("#reply_text").html("Por favor, tente novamente. Problema no envio da mensagem: " + textStatus);
		});
		
		return false;
	});
	
	$('.fadein img:gt(0)').hide();
	setInterval(function() {
		$('.fadein :first-child').hide()
			.next('img').fadeIn()
			.end().appendTo('.fadein');}, 
			4000);

});
