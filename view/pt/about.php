<!DOCTYPE html>
<html>
<head>
	<?php include 'common_head.php'; ?>
	<link rel="stylesheet" type="text/css" href="./css/about.css">
</head>
<body>
	<?php include 'before_body.php'; ?>
	
<div class="content blue_background centeredbox">
	<h1>A Empresa</h1>
	<div class="about-item">
   		<img src="./image/about_us.jpg" />
		<p>A <strong>SelsanTech</strong> é uma empresa que surgiu pela necessidade de produção tecnológica para melhorar e aprimorar processos de automação. A empresa reúne profissionais altamente qualificados para oferecer produtos e soluções sob medida para a indústria e o setor de serviços.</p>
		<p>Em atuação desde 2012, a <strong>SelsanTech</strong> possui expertise no desenvolvimento de hardware e software especializados. Sua equipe já desenvolveu projetos na área de inteligência computacional, segurança da informação, visão computacional e automação comercial.</p>
   		<br/>
		<p>Dr. Antonio Henrique Pinto Selvatici
		<br/><strong><a href="mailto:antoniohps@selsantech.com">antoniohps@selsantech.com</a></strong></p>
		<p>Dr. Humberto Sandmann
		<br/><strong><a href="mailto:humberto@selsantech.com">humberto@selsantech.com</a></strong></p>
   	</div>
   	<?php include 'contact.php'; ?>
</div>
	<?php include 'after_body.php'; ?>
</body>
</html>
