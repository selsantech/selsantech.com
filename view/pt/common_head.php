<meta http-equiv="content-type" content="text/html;charset=utf-8">
<meta http-equiv="content-language" content="pt-BR">
<meta http-equiv="cache-control" content="no-cache">

<title>SelsanTech</title>

<link rel="stylesheet" type="text/css" href="./css/default.css">
<link rel="stylesheet" type="text/css" href="./css/light.css"  />
<link rel="stylesheet" type="text/css" href="./css/dark.css"  />
<link rel="stylesheet" type="text/css" href="./css/transparent.css"  />
<link rel="stylesheet" type="text/css" href="./css/cssmenu.css"  />

<link rel="icon" type="image/png" href="./image/favicon.png" />

<script src="./js/jquery-2.1.0.min.js"></script>
<script src="./js/common.js"></script>
