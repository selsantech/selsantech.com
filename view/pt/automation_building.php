<!DOCTYPE html>
<html>
<head>
	<?php include 'common_head.php'; ?>
</head>
<body>
	<?php include 'before_body.php'; ?>
<div class="content blue_background centeredbox">
	<h1><a href="automation">Automação</a> > Predial</h1>
	<p>Nossa solução está na fabricação de produtos, protocolos e serviços que tornem o processo de instalação de um sistema de automação algo simples, seguro e confiável.</p>   
	
	<h3>Salas e corredores</h3>
	<p>O conceito plug-and-play é um ponto central do nosso paradigma de atuação. Com nossa plataforma é possível integrar e controlar:</p>
	<ul>
		<li>Iluminação ambiente</li>
		<li>Ar condicionado</li>
		<li>Persianas e janelas</li>
		<li>Controles remotos</li>
		<li>Portas e controle de acesso</li>
		<li>Áudio e vídeo</li>
		<li>Circuito fechado de TV</li>
		<li>Sensores de portas e janelas</li>
		<li>Controle de temperatura</li>
	</ul>
	
	<h3>Sistema de controle</h3>
	<p>O controle e monitoramento de todos os ambientes automatizados podem ser feitos em tempo real e através de um único sistema, onde é possível a tomada de decisão sobre cada item de forma precisa e ágil. Esse sistema de controle pode ser implementado em dois cenários:</p>
	<ul>
		<li><strong>Centro de controle virtual</strong>, tratando de um sistema alocado na "nuvem", num ambiente onde o controle fica dependente de um link de internet. Esse sistema oferece um baixo custo de instalação, além de permitir contar com manutenção remota assistida.</li>
		<li><strong>Sala de controle</strong>, um ambiente fisicamente instalado <i>in loco</i> ou em alguma outra localidade. Essa solução permite que o sistema seja monitorado e gerenciado sem interrupções externas, tais como queda de energia, falha de comunicação com a internet e outras. Neste cenário, o acesso também pode ser feito de maneira remota se desejado.</li>
	</ul>
	<p>Ambos os cenários dispõem de aplicativos para gerenciamento através de smartphones e tablets, bastando configurar a rede de acesso.</p>

	<h3>Rastreamento em tempo real - ZigTrack&reg</h3>
	<p>Como toda a tecnologia de integração e solução de software é feita pela <strong>SelsanTech</strong>, podemos incorporar tecnologias inovadoras ao sistema de monitoramento. Dentre nossos produtos inovadores está o <strong>ZigTrack&reg</strong>, que é uma tecnologia criada em parceira com a FAPESP (Fundação de Amparo à Pesquisa do Estado de São Paulo) para localização e inventariado em tempo real de equipamentos, contêineres e outros em geral, de baixo e/ou alto valor agregado.</p>
	<div style="text-align: center;">
		<iframe width="640" height="360" src="//www.youtube.com/embed/0WR1nH_0imE" frameborder="0" allowfullscreen></iframe>
		<iframe width="480" height="360" src="//www.youtube.com/embed/CWZRgH_Wrh0" frameborder="0" allowfullscreen></iframe>
		<br><small>Localização em tempo real do carrinho de carga. Vídeo gravado na Thermotech, Limeira/SP.</small>  
	</div> 
	<p>Com o <strong>ZigTrack&reg</strong> é possível aumentar a segurança do ambiente e otimizar o tempo de busca de equipamentos que são cruciais para o funcionamento do negócio.</p> 
	
	<h3>Cenas e programação</h3>
	<p>Você pode planejar todo o ambiente para seu melhor uso. Através das funções de programação é possível inclusive definir cenários, variando a iluminação, temperatura e outros parâmetros do ambiente de acordo com o horário do dia ou de uma demanda especifíca.</p>
	
	<h3>Custos de instalação e manutenção reduzidos</h3>
	<p>Em geral, as soluções de automação predial existentes são implementadas através de cabeamentos complexos. Já a nossa solução utiliza equipamentos de tecnologia de rede wifi, sem cabos específicos de controle. Assim, oferecemos uma economia real na instalação do sistema de automação, proporcionando:</p>
	<ul>
		<li><strong>Manutenção da integridade do ambiente, sem a necessidade de reformas</strong></li>
		<li><strong>Redução significativa do cabeamento necessário</strong></li>
		<li><strong>Redução da mão de obra para instalação</strong></li>
		<li><strong>Fácilidade de instalação e manutenção</strong></li>
	</ul>
	
	<h3>Internet</h3>
	<p>Através do serviço <strong>ZigPoint&reg</strong> todos os sistemas automatizados se tornam gerenciáveis pela internet, podendo inclusive delegar tarefas e definir papéis de usuários. Uma das funcionalidades por exemplo é a de delegar o controle de cada ambiente a um usuário específico por um determinado período de tempo, permitindo a um hóspede de um hotel, paciente de um hospital ou gerente de um escritório personalizar seu quarto ou sala pelo simples uso de um aplicativo no celular.</p>
	
	<h3>Servidor de gerenciamento e monitoramento</h3>
	<p>O grande diferencial das nossas soluções está no desenvolvimento do software servidor, que permite gerenciar toda a integração dos sistemas e monitorá-los remotamente.</p>
	<p>O nosso sistema servidor possui redundância e é implementado utilizando as técnicas mais avançadas de análise de dados, a fim de alertar e previnir não conformidades. Todos os registros históricos de funcionamento individualizado dos itens automatizados ficam disponíveis para auditorias e análises de performances, sempre visando o aperfeiçoamento do sistema e políticas de uso consciente de recursos.</p>
	
   	<?php include 'contact.php'; ?>
</div>
	<?php include 'after_body.php'; ?>
</body>
</html>
