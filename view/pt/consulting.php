<!DOCTYPE html>
<html>
<head>
	<?php include 'common_head.php'; ?>
	<link rel="stylesheet" type="text/css" href="./css/consulting.css">
	<script src="./js/sliderpane.js"></script>
</head>
<body>
	<?php include 'before_body.php'; ?>
<div class="content blue_background">
	<h1>Consultoria Avançada em Computação</h1>
	<div id="consulting" class="centeredbox">
		<div class="info-box intelligence" onmouseover="slideText('intelligence')" onmouseout="unslideText('intelligence')">
			<h3 class="info-title">Inteligência Computacional</h3>
    		<div id="intelligence-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="intelligence-text" class="hover-text" style="margin: 0px;"> 
    		<ul class=info-text>
    		    <li>Redes neurais artíficiais</li>
	    		<li>Suporte à tomada de decisão</li>
	    		<li>Aprendizado de máquina</li>
	    		<li>Otimização de processos</li>
   	    	</ul></div>    	
		</div><div class="info-box security" onmouseover="slideText('security')" onmouseout="unslideText('security')">
			<h3 class="info-title" >Segurança da Informação</h3>
    		<div id="security-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="security-text" class="hover-text" style="margin: 0px;"> 
			<ul class="info-text">
			    <li>Encriptação de dados</li>
				<li>Sistemas de PKI (Public-Key Infrastructure)</li>
				<li>Certificados e assinaturas digitais</li>
				<li>Comunicação segura</li>
			</ul></div>
   		</div><div class="info-box vision" onmouseover="slideText('vision')" onmouseout="unslideText('vision')">
			<h3 class="info-title">Visão Computacional</h3>
    		<div id="vision-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="vision-text" class="hover-text" style="margin: 0px;"> 
			<ul class="info-text">
			    <li>Reconhecimento de objetos</li>
				<li>Rastreamento de objetos em video</li>
				<li>Mapeamento 3D de ambientes</li>
				<li>Sistemas de controle baseado em visão</li>
			</ul></div>
   		</div><div class="info-box bigdata" onmouseover="slideText('bigdata')" onmouseout="unslideText('bigdata')">
		    <h3 class="info-title">Big Data</h3>
       		<div id="bigdata-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="bigdata-text" class="hover-text" style="margin: 0px;"> 
			<ul class="info-text">
				<li>Recuperação inteligente de informações</li>
				<li>Deep Learning</li>
				<li>Soluções customizadas a partir de modelos matemáticos </li>
			</ul></div>
		</div>
   	</div>
	<h1>Consultoria</h1>
	<div id="consulting" class="centeredbox">
		<div class="info-box softeng" onmouseover="slideText('softeng')" onmouseout="unslideText('softeng')">
		    <h3 class="info-title">Engenharia de Software</h3>
       		<div id="softeng-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="softeng-text" class="hover-text" style="margin: 0px;"> 
			<ul class="info-text">
				<li>Modelagem de Banco de Dados</li>
				<li>Plataformas Empresariais</li>
				<li>Sistemas Embarcados</li>
				<li>Sistemas de Tempo Real</li>
				<li>Metodologia PMBOK</li>
			</ul> </div>
		</div><div class="info-box mobile" onmouseover="slideText('mobile')" onmouseout="unslideText('mobile')">
		    <h3 class="info-title">Smartphones</h3>
       		<div id="mobile-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="mobile-text" class="hover-text" style="margin: 0px;"> 
			<ul class="info-text">
				<li>Aplicações Móveis Customizadas</li>
				<li>iPhone e Android</li>
				<li>Integração com Soluções Web e Desktop</li>
			</ul> </div>
		</div>
	</div>
   	<?php include 'contact.php'; ?>
</div>
	<?php include 'after_body.php'; ?>
</body>
</html>
