<!DOCTYPE html>
<html>
<head>
	<?php include 'common_head.php'; ?>
	<link rel="stylesheet" type="text/css" href="./css/solution.css">
</head>
<body>
	<?php include 'before_body.php'; ?>
<div class="content blue_background centeredbox">
	<h1>Produtos e Soluções</h1>
   	<p>Além do serviço de consultoria, Selsantech também desenvolve produtos e soluções off-the-shelf, com o uso de hardware e software próprios</p>
	<div class="solution-item" style="margin-top: 20px;">
		<div class="logo">
			<img src="./image/product_zigbee_module.png" />
		</div>
   		<h2>Módulo ZigBee</h2>
   		<p>O módulo Zigbee SSTM4 é uma solução embarcável de comunicação sem fio de baixo custo e baixo consumo, utilizando o SoC CC2530F256 da Texas Instruments, compatível com o software ZStack. O módulo possui um rádio IEEE 802.15.4 controlado por um processador 8051 com 32MHz de clock, combinando bom poder de processamento com baixo consumo de energia. O módulo vem ainda com dois periféricos integrados, permitindo a criação de dispositivos embarcados sem a necessidade de outros módulos.</p>
   		<p><a href="zigbee"><button>Veja mais</button></a></p>
   	</div>
   	<div class="solution-item" style="margin-top: 20px;">
		<div class="logo">
   			<img src="./image/product_saidafacil.png" />
   		</div>
		<h2>Saida Fácil</h2>
   		<p>Uma solução desenvolvida para aumentar a segurança e agilidade na entrada e saída de alunos de escolas. O <b>Saída Fácil</b> monitora a aproximação dos responsáveis de um aluno em relação ao perímetro da escola, permitindo um melhor planejamento na logística e facilitando o trânsito em seu entorno. Solicite uma visita.</p>
		<p><a href="http://www.saidafacil.com/" target="_blank"><button>Visite o Website</button></a></p>
   	</div>
   	<div class="solution-item">
		<div class="logo">
   			<img src="./image/product_zigtrack.png" />
   		</div>
   		<h2>ZigTrack</h2>
   		<p>Uma rede de dispositivos baseada no protocolo ZigBee e RFID para localização de pessoas e/ou objetos em ambientes internos ou externos. Trata-se de projeto de P&D, desenvolvido com financiamento da Fundação de Amparo à Pesquisa do Estado de São Paulo, <strong>FAPESP</strong>.</p>
		<p><a href="http://www.zigtrack.com/" target="_blank"><button>Visite o Website</button></a></p>
	</div>
   	<div class="solution-item">
   		<div class="logo">
   			<div class="fadein">
	   			<img src="./image/product_fromstation.png" />
	    		<img src="./image/product_fromstation_box.png" />
    		</div>
		</div>
   		<h2>fromStation</h2>
   		<p>O fromStation transmite áudio de qualquer TV para um smartphone, permitindo os clientes de bares, e lugares públicos em geral, ouvir shows, programas e jogos sem a preocupação com ruídos externos. Aumente o número de clientes com esse diferencial tecnológico em seu estabelecimento.</p>
		<p><a href="http://www.fromstation.com/" target="_blank"><button>Visite o Website</button></a></p>
   	</div>
   	<?php include 'contact.php'; ?>
</div>
	<?php include 'after_body.php'; ?>
</body>
</html>
