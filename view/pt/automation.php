<!DOCTYPE html>
<html>
<head>
	<?php include 'common_head.php'; ?>
	<link rel="stylesheet" type="text/css" href="./css/automation.css">
	<script src="./js/sliderpane.js"></script>
</head>
<body>
	<?php include 'before_body.php'; ?>
<div class="content blue_background">
	<h1>Automação, integração e monitoramento</h1>
   	<p>A <strong>Selsantech</strong> é especializada em conectividade da <strong>Internet das Coisas (IoT)</strong>, integrando produtos inteligentes de fabricação própria e/ou de terceiros.</p>
	<div id="automation" class="centeredbox">
		<a href="residential"><div class="info-box residence" onmouseover="slideText('residence')" onmouseout="unslideText('residence')">
			<h3 class="info-title">Residencial</h3>
    		<div id="residence-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="residence-text" class="hover-text" style="margin: 0px;"> 
    		<ul class=info-text>
    		    <li>Iluminação ambiente, controle de temperatura, etc</li>
	    		<li>Programação e criação de cenas</li>
	    		<li>Obras e cabeamentos reduzidos</li>
	    		<li>Controle via internet</li>
   	    	</ul> </div>    	
   		</div></a><a href="building"><div class="info-box building" onmouseover="slideText('building')" onmouseout="unslideText('building')">
			<h3 class="info-title">Predial</h3>
    		<div id="building-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="building-text" class="hover-text" style="margin: 0px;"> 
			<ul class="info-text">
				<li>Monitoramento e gerenciamento de ambientes em tempo real</li>
				<li>Rastreamento de equipamentos e produtos em tempo real - ZigTrack&reg</li>
				<li>Hotéis, hospitais, aeroportos, etc</li>
			</ul></div>
   		</div></a><a href="smartcities"><div class="info-box smartcities" onmouseover="slideText('smartcities')" onmouseout="unslideText('smartcities')">
			<h3 class="info-title">Cidades inteligentes</h3>
       		<div id="smartcities-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="smartcities-text" class="hover-text" style="margin: 0px;"> 
			<ul class="info-text">
				<li>Iluminação pública, sinalização de transito, sistema de emergência, etc</li>
				<li>Monitoramento remoto e central de controle</li>
				<li>Auditoria de concessão pública</li>
			</ul> </div>
		</div></a>
   	</div>
   	<?php include 'contact.php'; ?>
</div>
	<?php include 'after_body.php'; ?>
</body>
</html>
