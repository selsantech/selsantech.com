<!DOCTYPE html>
<html>
<head>
	<?php include 'common_head.php'; ?>
</head>
<body>
	<?php include 'before_body.php'; ?>
<div class="content blue_background centeredbox">
	<h1><a href="automation">Automação</a> > Residencial</h1>
	<p>Nossa solução está na fabricação de produtos, software e serviços que tornem o processo de instalação de um sistema de automação algo simples, seguro e confiável.</p>   
	
	<h3>Integração</h3>
	<p>O conceito plug-and-play é um ponto central do nosso paradigma de atuação. Você pode controlar:</p>
	<ul>
		<li>Iluminação ambiente</li>
		<li>Ar condicionado</li>
		<li>Persianas e janelas</li>
		<li>Controles remotos</li>
		<li>Portas e controle de acesso</li>
		<li>Áudio e vídeo</li>
		<li>Circuito fechado de TV</li>
		<li>Sensores de portas e janelas</li>
		<li>Banheiros: chuveiros e banheiras</li>
	</ul>
	
	<h3>Cenas e programação</h3>
	<p>Você pode planejar todo o ambiente para seu melhor uso, personalizando-o de forma simples. Por exemplo, você poderá preparar a iluminação da sala de TV para um jantar especial no horário desejado, simplesmente apertando um botão.</p>
	
	<h3>Custos de instalação e manutenção reduzidos</h3>
	<p>Em geral, as soluções de automação residencial são implementadas através de complexos cabeamentos. Já a nossa solução utiliza equipamentos com tecnologias wifi, sem cabos de controle. Assim, oferecemos uma economia real na instalação de seu sistema de automação, proporcionando:</p>
	<ul>
		<li><strong>Manutenção da integridade do ambiente, sem a necessidade de reformas</strong></li>
		<li><strong>Redução significativa do cabeamento necessário</strong></li>
		<li><strong>Redução da mão de obra para instalação</strong></li>
		<li><strong>Facilidade de instalação</strong></li>
	</ul>
	
	<h3>Internet</h3>
	<p>Através do serviço <strong>ZigPoint&reg</strong> é possível fazer o monitoramento em tempo real de todos os equipamentos da sua rede de automação residencial através da internet.</p>
	
	<h3>Software de gerenciamento e monitoramento</h3>
	<p>O grande diferencial das nossas soluções está no desenvolvimento do software servidor, que permite gerenciar toda a integração dos sistemas e monitorá-los remotamente, sem a necessidade de configurações especiais do roteador da internet.</p>
	
   	<?php include 'contact.php'; ?>
</div>
	<?php include 'after_body.php'; ?>
</body>
</html>
