<div id="contact">
<h1>Contato</h1>
<p>A <strong>SelsanTech</strong> é uma empresa com uma grande experiência em automação, inteligência computacional e aprendizado de máquinas. Entre em contato conosco através do formulário abaixo, e assim poderemos oferecer soluções inovadoras para seu problema, produto ou negócio. Se preferir, entre em contato através de <strong><a href="mailto:contato@selsantech.com">contato@selsantech.com</a></strong>.</p>
<form id="contact-form" method="post">
	<input type="text" name="Name" id="Name" placeholder="Nome" required="required" />
	<input type="text" name="Email" id="Email" placeholder="E-mail" />
	<input type="text" name="Phone" id="Phone" placeholder="Telefone" />
	<textarea name="Message" rows=10 cols=30 id="Message" placeholder="Mensagem"></textarea>
	<button id="contact-button"/>Enviar</button>
	<span id=reply_text></span>
</form>
<div>
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.5649649926395!2d-46.65178668502164!3d-23.584062984671608!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5a53e69e0def%3A0x48dde1babf720d71!2sSelsanTech!5e0!3m2!1spt!2sbr!4v1487161783382" width="450" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
	<p>Rua Pelotas, 753 - sala 7</p>
	<p>Vila Mariana, São Paulo</p>
	<p>04012-002 - SP - Brasil</p>
	<br>
	<p><strong>Selsan Tecnologia Industrial e Comercial LTDA - ME</strong></p>
	<p><small>CNPJ 18.054.402/0001-34</small></p>
</div>
</div>
