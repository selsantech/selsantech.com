<!DOCTYPE html>
<html>
<head>
	<?php include 'common_head.php'; ?>
	<script>(function(d, s, id) {
  		var js, fjs = d.getElementsByTagName(s)[0];
  		if (d.getElementById(id)) return;
  		js = d.createElement(s); js.id = id;
  		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
  		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
</head>
<body>
	<?php include 'before_body.php'; ?>
<div class="content blue_background centeredbox">
	<h1><a href="solution">Produtos</a> > Módulo ZigBee</h1>
	<div id="fb-root"></div>
	<span class="rightarea">
		<img src="./image/zigbee_black.jpg"/>
		<h2>de <del>R$ 65,00</del> por <span class="offer">R$ 50,00</span></h2>
		<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post"><input type="hidden" name="cmd" value="_s-xclick"><input type="hidden" name="hosted_button_id" value="YW6SJCJAJ5TU6"><input type="image" src="https://www.paypalobjects.com/pt_BR/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - A maneira fácil e segura de enviar pagamentos online!"></form>
		<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post" ><input type="hidden" name="cmd" value="_s-xclick"><input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIG1QYJKoZIhvcNAQcEoIIGxjCCBsICAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYAVz8jGHzH+2QNt6XOzoZVvyexr9tHCo7uMZyvobCgdYFPGuYADNcNpBpKYyG/3L0YQoR8/V/A77L4w5Et1kOfDUKeZFm9x5Viwqzw7CCf3tyi3CW1IVcEBtLhlp8ZQ3YYujj3ac1rzqipgzW8RJ9riA7XDGfJOdANlcuJeBq+1NzELMAkGBSsOAwIaBQAwUwYJKoZIhvcNAQcBMBQGCCqGSIb3DQMHBAicz1oghe1huIAw3Mr1noGVnEPsaRz0vZ+gI8tOWNn1hln6Aq+9c7/Smb+QJk0XdSrTLE6GZXx70f+eoIIDhzCCA4MwggLsoAMCAQICAQAwDQYJKoZIhvcNAQEFBQAwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMB4XDTA0MDIxMzEwMTMxNVoXDTM1MDIxMzEwMTMxNVowgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBR07d/ETMS1ycjtkpkvjXZe9k+6CieLuLsPumsJ7QC1odNz3sJiCbs2wC0nLE0uLGaEtXynIgRqIddYCHx88pb5HTXv4SZeuv0Rqq4+axW9PLAAATU8w04qqjaSXgbGLP3NmohqM6bV9kZZwZLR/klDaQGo1u9uDb9lr4Yn+rBQIDAQABo4HuMIHrMB0GA1UdDgQWBBSWn3y7xm8XvVk/UtcKG+wQ1mSUazCBuwYDVR0jBIGzMIGwgBSWn3y7xm8XvVk/UtcKG+wQ1mSUa6GBlKSBkTCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb22CAQAwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOBgQCBXzpWmoBa5e9fo6ujionW1hUhPkOBakTr3YCDjbYfvJEiv/2P+IobhOGJr85+XHhN0v4gUkEDI8r2/rNk1m0GA8HKddvTjyGw/XqXa+LSTlDYkqI8OwR8GEYj4efEtcRpRYBxV8KxAW93YDWzFGvruKnnLbDAF6VR5w/cCMn5hzGCAZowggGWAgEBMIGUMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbQIBADAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTQwNTI5MTkzNDU5WjAjBgkqhkiG9w0BCQQxFgQUT9YkwGFMB6S2MCbUvHUGkLir9vswDQYJKoZIhvcNAQEBBQAEgYCwkCcXJ8XbwoY/JRS5IR+17jWacQv8fjqANYN0xZn5ueLlsQxUP2+8EnTuvCyyTBoqQR5Mfy67WnE1nf/kdDB6raafYBJquASZQ0DZfQMBxiuH6GpqsMYtgCEEOLZs6brMHyae7HHL0r5Ep8bcAtfTkmZhcyuYrCE2xvCnvkUppg==-----END PKCS7-----"><input type="image" src="https://www.paypalobjects.com/pt_BR/i/btn/btn_viewcart_SM.gif" border="0" name="submit" alt="PayPal - A maneira fácil e segura de enviar pagamentos online!"></form>
	</span>
	<p>O módulo Zigbee SSTM4 é uma solução embarcável de comunicação sem fio de baixo custo e baixo consumo, utilizando o SoC CC2530F256 da Texas Instruments, compatível com o software ZStack. O módulo possui um rádio IEEE 802.15.4 controlado por um processador 8051 com 32MHz de clock, combinando bom poder de processamento com baixo consumo de energia. O módulo vem ainda com dois periféricos integrados, permitindo a criação de dispositivos embarcados sem a necessidade de outros módulos. </p>
	<p>A <strong>SelsanTech</strong> projeta e comercializa outros módulos ZigBee para fornecer comunicação do tipo <i>mesh</i> entre dispositivos, incluindo seus periféricos. Caso precise de customizações no módulo ou de outros periféricos, escreva para <a href="mailto:comercial@selsantech.com">comercial@selsantech.com</a></p>
	<h4>Especificações técnicas</h4>
	<ul>
		<li>Alimentação: 2 V - 3.6 V </li>				
		<li>Periféricos: push-button e led </li>
		<li>Processador: arquitetura 8051 com 32 Mhz de clock </li>
		<li>Memória: 8 KB de RAM e Flash de 256 KB</li>				
		<li>Frequência de transmissão programável na faixa de 2.4 GHz: de 2394 a 2507 MHz</li>
		<li>Potência de transmissão programável: -22 a +4.5 dBm</li>
		<li>Sensibilidade do receptor: -97 dBm</li>
		<li>Alcance Máximo estimado: até 120m (+4.5 dBm)</li>
		<li>Topologias de rede: P-to-P, P-to-M, ZigBee/Mesh</li>
		<li>Consumo em modo sleep: 0.4 µA</li>
		<li>Consumo no modo ativo (led apagado): 24 mA</li>
		<li>Consumo no modo de transmissão (+1 dBm, led apagado): 29 mA</li>
		<li>Taxa de transmissão de dados: 250 Kbps</li>
		<li>Segurança: 128-bit AES</li>
		<li>Portas de comunicação: GPIO (19 canais), SPI e UART</li>
		<li>Entradas analógicas: ADC de 8 canais, com monitoramento da bateria e da temperatura interna</li>
	</ul>
	<h4>Dimensões e pinagem</h4>
	<img src="./image/zigbee_dimension.png"/>
	<img src="./image/pinagem_rev_a_c.png"/>
	<img src="./image/pads_layout.png"/>
	<!-- 
			<h4>Instruções de Uso</h4>
-->
	<h4>Bibliografia</h4>
	<ul>
		<li>REDES MESH. In: WIKIPÉDIA, a enciclopédia livre. Flórida: Wikimedia Foundation, 2014. Disponível em: <a href="http://pt.wikipedia.org/w/index.php?title=Redes_Mesh&oldid=38890167" target="_blank">http://pt.wikipedia.org/w/index.php?title=Redes_Mesh&oldid=38890167</a>. Acesso em: 29 maio 2014. </li>
		<li>ZIGBEE. In: WIKIPÉDIA, a enciclopédia livre. Flórida: Wikimedia Foundation, 2013. Disponível em: <a href="http://pt.wikipedia.org/w/index.php?title=ZigBee&oldid=37207891" target="_blank">http://pt.wikipedia.org/w/index.php?title=ZigBee&oldid=37207891</a>. Acesso em: 29 maio 2014. </li>
	</ul>
	<h4>Comentários</h4>
	<div class="fb-comments" data-href="http://selsantech.com/zigbeemodule/coments" data-numposts="5" data-colorscheme="light"></div>
   	<?php include 'contact.php'; ?>
</div>
	<?php include 'after_body.php'; ?>
</body>
</html>
