<!DOCTYPE html>
<html>
<head>
	<?php include 'common_head.php'; ?>
	<link rel="stylesheet" type="text/css" href="./css/case.css">
</head>
<body>
	<?php include 'before_body.php'; ?>
<div class="content blue_background">
	<h1>Casos de sucesso</h1>
	<div class="case-item">
   		<img src="./image/case_catsearch.png" />
		<h2>CatSearch</h2>
   		<p>O processo de busca e localização de um registro dentro de um banco de dados pode definir o custo de uma tomada de decisão. O sistema  <b>CatSearch</b> tem como finalidade localizar registros com base em uma descrição aproximada, em liguagem natural, fazendo uso de inteligência computacional. Como se trata de uma busca com aprendizado de máquina, há um bom desempenho na localização de registros mesmo que haja cadastros classificados erroneamente na base de aprendizado. Essa solução foi desenvolvida baseando-se nos melhores e mais modernos conceitos matemáticos aplicados à técnicas inteligentes para tomadas de decisões.</p>
   	    <p><strong>Cliente:</strong> <a href="http://www.systax.com.br" target="_blank"> Systax</a></p>
   	    <p><strong>Assuntos:</strong> Inteligência Computacional, Big Data</p>
	</div>
   	<?php include 'contact.php'; ?>
</div>
	<?php include 'after_body.php'; ?>
</body>
</body>
</html>
