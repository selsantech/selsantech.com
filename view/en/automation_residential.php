<!DOCTYPE html>
<html>
<head>
	<?php include 'common_head.php'; ?>
</head>
<body>
	<?php include 'before_body.php'; ?>
<div class="content blue_background centeredbox">
	<h1><a href="automation">Automation</a> > Home</h1>
	<p>Our solution comprises the development of devices, software and services that turn the deployment of automation systems into a simple, reliable and secure process.</p>   
	
	<h3>Integration</h3>
	<p>The "plug-and-play" paradigm is a key concept in our business, allowing you to control:</p>
	<ul>
		<li>Ambient lighting</li>
		<li>Air conditioning</li>
		<li>Widows and closures</li>
		<li>Infrared controlled devices</li>
		<li>Door opening and access authorization</li>
		<li>Audio and video</li>
		<li>Closed-circuit TV</li>
		<li>Door and window sensors</li>
		<li>Shower and bathtub</li>
	</ul>
	
	<h3>Scene programming</h3>
	<p>You can setup the ambient for your best comfort, customizing it in a simple way. For instance, you can adjust your dining room for a special dinner by simply pressing a button.</p>
	
	<h3>Reduced installation and maintenance costs</h3>
	<p>Traditional home automation solutions use complex cabling. Our solution is based on wireless technology, without control cables. Thus, we can offer you a more economical home automation system, resulting in: </p>
	<ul>
		<li><strong>Environment integrity; no need for renovation</strong></li>
		<li><strong>Significant cabling reducing</strong></li>
		<li><strong>Less workforce required</strong></li>
		<li><strong>Ease of installation</strong></li>
	</ul>
	
	<h3>Internet</h3>
	<p><strong>ZigPoint&reg</strong> service provides real-time on-line monitoring and control of all home automation devices through the internet.</p>
	
	<h3>Monitoring and control software</h3>
	<p>Our solution's biggest advantage resides in the server software, that enables monitoring and managing all systems remotely, without any special configuration of the internet router.</p>
	
   	<?php include 'contact.php'; ?>
</div>
	<?php include 'after_body.php'; ?>
</body>
</html>
