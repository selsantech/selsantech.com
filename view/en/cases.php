<!DOCTYPE html>
<html>
<head>
	<?php include 'common_head.php'; ?>
	<link rel="stylesheet" type="text/css" href="./css/case.css">
</head>
<body>
	<?php include 'before_body.php'; ?>
<div class="content blue_background">
	<h1>Success Cases</h1>
	<div class="case-item">
   		<img src="./image/case_catsearch.png" />
		<h2>CatSearch</h2>
   		<p>The process of register look-up in a database can define the cost of making a decision. <b>CatSearch</b> performs database queries based on non-exact natural language descriptions using computational intelligence. Since it performs machine learning from large data-sets, misclasified entries in the learning base does not impact the retrieval performance so badly. This solution was developed based on the best and modern mathematics concepts applied to a smart technique of decision-making.</p>
   	    <p><strong>Client:</strong> <a href="http://www.systax.com.br" target="_blank"> Systax</a></p>
   	    <p><strong>Topics:</strong> Computer Intelligence, Big Data</p>
	</div>
   	<?php include 'contact.php'; ?>
</div>
	<?php include 'after_body.php'; ?>
</body>
</body>
</html>
