<div id="contact">
<h1>Contact Us</h1>
<p><strong>SelsanTech</strong> is a company with experience in advanced areas of Computer Science. If you need innovative solutions for your problem, product or bussiness, use the form below to contact us. If you prefer, send an e-mail to <strong><a href="mailto:contact@selsantech.com">contact@selsantech.com</a></strong>.</p>
<form id="contact-form">
	<input type="text" name="Name" id="Name" placeholder="Name" required="required" />
	<input type="text" name="Email" id="Email" placeholder="E-mail" />
	<input type="text" name="Phone" id="Phone" placeholder="Phone" />
	<textarea name="Message" rows=10 cols=30 id="Message" placeholder="Message"></textarea>
	<button id="contact-button"/>Submit</button>
	<span id=reply_text></span>
</form>
<div>
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.5649649926395!2d-46.65178668502164!3d-23.584062984671608!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5a53e69e0def%3A0x48dde1babf720d71!2sSelsanTech!5e0!3m2!1sen!2sus!4v1487161783382" width="450" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
	<p>Rua Pelotas, 753 - office 7</p>
	<p>Vila Mariana, São Paulo</p>
	<p>04012-002 - SP - Brazil</p>
	<br>
	<p><strong>Selsan Tecnologia Industrial e Comercial LTDA - ME</strong></p>
	<p><small>CNPJ 18.054.402/0001-34</small></p>
</div>
</div>
