<!DOCTYPE html>
<html>
<head>
	<?php include 'common_head.php'; ?>
</head>
<body>
	<?php include 'before_body.php'; ?>
<div class="content blue_background centeredbox">
	<h1><a href="automation">Automação</a> > Predial</h1>
	<p>Our solution comprises the development of devices, software and services that turn the deployment of automation systems into a simple, reliable and secure process.</p>   
	
	<h3>Rooms and corridors</h3>
	<p>The "plug-and-play" paradigm is a key concept in our business. Our platform can integrate and control:</p>
	<ul>
		<li>Ambient lighting and temperature</li>
		<li>Air conditioning</li>
		<li>Widows and closures</li>
		<li>Infrared controlled devices</li>
		<li>Door opening and access authorization</li>
		<li>Audio and video</li>
		<li>Closed-circuit TV</li>
		<li>Door and window sensors</li>
	</ul>
	
	<h3>Control system</h3>
	<p>Supervision and control of all automated ambients can be done on-line and in real-time using a single interface, allowing agile and precise decision making over each parameter, such as lighting or temperature. The control system comes in two versions:</p>
	<ul>
		<li><strong>Virtual Control Center</strong>, a cloud-based system, which depends on an internet link. It has low-cost  installation and maintenance, allowing for remote assistance.</li>
		<li><strong>Control Room</strong>, a physically installed environment, <i>in loco</i> or apart from the smart building. This solution allows monitoring and control with no dependance on exogenous factors, such as an internet link. This version can work together with the Virtual Control Center as well.</li>
	</ul>
	<p>Both versions come with mobile applications for local and remote system management.</p>

	<h3>Real-time tracking - ZigTrack&reg</h3>
	<p>Once <strong>Selsantech</strong> dominates both the hardware technology and the software solution, we can incorporate inovative technologies to the monitor system. <strong>ZigTrack&reg</strong> is among these inovative technologies, consisting of a real-time location and inventory system developed with funds from FAPESP (the São Paulo State Research Funding Foundation), being applicable to the tracking of containers, pieces of equipment, and other items.</p>
	<div style="text-align: center;">
		<iframe width="640" height="360" src="//www.youtube.com/embed/0WR1nH_0imE" frameborder="0" allowfullscreen></iframe>
		<iframe width="480" height="360" src="//www.youtube.com/embed/CWZRgH_Wrh0" frameborder="0" allowfullscreen></iframe>
		<br><small>Trolley real-time localization. Footage made at Thermotech, Limeira/SP.</small>  
	</div> 
	<p><strong>ZigTrack&reg</strong> can improve in-site security and minimize the time for localizing the equipament required for keeping your business running.</p> 
	
	<h3>Scene programming</h3>
	<p>You can setup the ambient for its best use, customizing it in a simple way. The programming functionality enables the specification of several ambient parameters, such as lighting and temperature, according to a time-table or at any time so as to fulfill a specific requirement.</p>
	
	<h3>Reduced installation and maintenance costs</h3>
	<p>Traditional building automation solutions use complex cabling. Our solution is based on wireless technology, without control cables. Thus, we can offer you a more economical home automation system, resulting in: </p>
	<ul>
		<li><strong>Environment integrity; no need for renovation</strong></li>
		<li><strong>Significant cabling reducing</strong></li>
		<li><strong>Less workforce required</strong></li>
		<li><strong>Ease of installation and maintenance</strong></li>
	</ul>
	
	<h3>Internet</h3>
	<p><strong>ZigPoint&reg</strong> service provides real-time on-line monitoring and control of all automation devices through the internet, allowing for different levels of authoritative access to the system. For instance, a hotel, hospital or enterprise building owner can authorize the room users to customize their specific environment using a smartphone app during a specific time interval.</p>
	
	<h3>Monitoring and control software</h3>
	<p>Our solution's biggest advantage resides in the server software, that enables monitoring and managing all systems remotely, without any special configuration of the internet router. Our smart servers complies with several information security standards and applies modern data analysis techniques so as to warn about unexpected situations, or avoiding them. Individual records about user actions or device functioning are kept available for system auditing and performance analisys.</p>
	
   	<?php include 'contact.php'; ?>
</div>
	<?php include 'after_body.php'; ?>
</body>
</html>
