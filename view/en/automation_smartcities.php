<!DOCTYPE html>
<html>
<head>
	<?php include 'common_head.php'; ?>
</head>
<body>
	<?php include 'before_body.php'; ?>
<div class="content blue_background centeredbox">
	<h1><a href="automation">Automação</a> > Cidades inteligentes</h1>
	<p>Nossa solução está na fabricação de produtos, protocolos e serviços que tornem fácil o processo de instalação e monitoramento de sistemas inteligentes.</p>
	<p>O conceito de cidade inteligente é bem amplo. Pensando neste aspecto, estamos focados na área de comunicação de curta distância utilizando redes mesh baseadas na especificação Zigbee e em servidores de controle, integração e monitoramento.</p>
	<p>Temos um leque de serviços que podem ser prestados para montagem de sistemas de controle em salas físicas e/ou na internet, através de alocação na "nuvem".</p>
	<p>O nosso sistema servidor possui redundância e é implementado utilizando as técnicas mais avançadas de análise de dados e predição de séries temporais (incluindo o uso das técnicas mais avançadas para computação inteligente e Big Data), a fim de alertar e previnir não conformidades. Todos os registros históricos de funcionamento individualizado dos itens automatizados ficam disponíveis para auditorias e análises de performance, sempre visando o aperfeiçoamento do sistema e políticas de uso consciente de recursos. Além disso, implementamos nosso controle sobre os mais diversos protocolos de comunicação.</p>
	<p>Atualmente, procuramos parceiros para desenvolver projetos pilotos nesta área. Por favor, entre em contato e vamos trabalhar juntos na criação de soluções.</p>
	<?php include 'contact.php'; ?>
</div>
	<?php include 'after_body.php'; ?>
</body>
</html>
