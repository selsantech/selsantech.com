<!DOCTYPE html>
<html>
<head>
	<?php include 'common_head.php'; ?>
	<link rel="stylesheet" type="text/css" href="./css/consulting.css">
	<script src="./js/sliderpane.js"></script>
</head>
<body>
	<?php include 'before_body.php'; ?>
<div class="content blue_background">
	<h1>Advanced Consulting</h1>
	<div id="consulting" class="centeredbox">
		<div class="info-box intelligence" onmouseover="slideText('intelligence')" onmouseout="unslideText('intelligence')">
			<h3 class="info-title">Computer Intelligence</h3>
    		<div id="intelligence-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="intelligence-text" class="hover-text" style="margin: 0px;"> 
    		<ul class=info-text>
    		    <li>Artificial Neural Networks</li>
	    		<li>Supported Decision Making</li>
	    		<li>Machine Learning</li>
	    		<li>Process Optimization</li>
	    	</ul></div>    	
		</div><div class="info-box security" onmouseover="slideText('security')" onmouseout="unslideText('security')">
			<h3 class="info-title" >Information Security</h3>
    		<div id="security-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="security-text" class="hover-text" style="margin: 0px;"> 
			<ul class="info-text">
			    <li>Data Encryption</li>
				<li>PKI (Public-Key Infrastructure)</li>
				<li>Digital Certificates and Signatures</li>
				<li>Secure Communication</li>
			</ul></div>
   		</div><div class="info-box vision" onmouseover="slideText('vision')" onmouseout="unslideText('vision')">
			<h3 class="info-title">Computer Vision</h3>
    		<div id="vision-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="vision-text" class="hover-text" style="margin: 0px;"> 
			<ul class="info-text">
			    <li>Object Recognition in Images</li>
				<li>Object Tracking in Video</li>
				<li>Environment 3D Mapping</li>
				<li>Vision-based Control Systems</li>
			</ul></div>
   		</div><div class="info-box bigdata" onmouseover="slideText('bigdata')" onmouseout="unslideText('bigdata')">
		    <h3 class="info-title">Big Data</h3>
       		<div id="bigdata-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="bigdata-text" class="hover-text" style="margin: 0px;"> 
			<ul class="info-text">
				<li>Intelligent Information Retrieval</li>
				<li>Deep Learning</li>
				<li>Custom Solutions based on Mathematical Models</li>
			</ul></div>
		</div><!-- <div class="info-box banking" onmouseover="slideText('banking')" onmouseout="unslideText('banking')">
		    <h3 class="info-title">Core Banking Architecture</h3>
       		<div id="banking-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="banking-text" class="hover-text" style="margin: 0px;"> 
			<ul class="info-text">
				<li>Account Management</li>
				<li>BIAN Specifications</li>
				<li>PCI Specifications</li>
			</ul></div>
		</div> -->
   	</div>
	<h1>Consulting</h1>
	<div id="consulting" class="centeredbox">
		<div class="info-box softeng" onmouseover="slideText('softeng')" onmouseout="unslideText('softeng')">
		    <h3 class="info-title">Software Engineering</h3>
       		<div id="softeng-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="softeng-text" class="hover-text" style="margin: 0px;"> 
			<ul class="info-text">
				<li>Database Modeling</li>
				<li>Enterprise Plataforms</li>
				<li>Embedded Systems</li>
				<li>Real Time Systems</li>
				<li>PMBOK Methodology</li>
			</ul></div>
		</div><div class="info-box mobile" onmouseover="slideText('mobile')" onmouseout="unslideText('mobile')">
		    <h3 class="info-title">Smartphones</h3>
       		<div id="mobile-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="mobile-text" class="hover-text" style="margin: 0px;"> 
			<ul class="info-text">
				<li>Custom Applications for Mobile</li>
				<li>iPhone and Android</li>
				<li>Integration with Web and Desktop Solutions</li>
			</ul></div>
		</div>
	</div>
   	<?php include 'contact.php'; ?>
</div>
	<?php include 'after_body.php'; ?>
</body>
</html>

