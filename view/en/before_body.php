<?php
	$v_params = $this->getParams();
	$v_page = $v_params['page']; 
?>
<header>
	<div class="content">
		<a href="home"><img src="./image/selsantech_en.png" class="selsantech_logo"/></a>
		<div class="right_box">
	    	<div class="language_area">
		    	<a href="language/pt" title="português" class="flag_brazil_transp"><span></span></a>
	    	</div>
	    	<div class="social_area">
				<a href="http://www.facebook.com/selsantech/" class="facebook_transp" title="facebook" target="_blank"><span></span></a>
		    	<a href="http://www.linkedin.com/company/selsantech" class="linkedin_transp" title="linkedin" target="_blank"><span></span></a>
		       	<a href="http://plus.google.com/+Selsantech/" class="googleplus_transp" title="google plus" target="_blank"><span></span></a>
		       	<a href="http://www.youtube.com/channel/UC0GUI2qPhSXToZ4pKqQnp_g" class="youtube_transp" title="youtube" target="_blank"><span></span></a>
		    </div>
		</div>
	</div>
</header>
<section class="dark_background">
	<div class="content">

   	<nav>
	<!-- ************ CSS MENU *************** -->
<div id='cssmenu'>
<ul>
	<li class="<?php if($v_page=='consulting'){echo 'active ';}?> first"> 
   		<a href='consulting'><span>Consulting</span></a>
	</li>
	<li class="<?php if($v_page=='cases'){echo 'active ';}?>">
		<a href='cases'><span>Sucess Cases</span></a>
	</li>
	<li class="<?php if($v_page=='automation'){echo 'active ';}?>">
		<a href='automation'><span>Automation</span></a>
	</li>
	<li class="<?php if($v_page=='solution'){echo 'active ';}?>">
		<a href='solution'><span>Products</span></a>
	</li>
	<li class="<?php if($v_page=='partner'){echo 'active ';}?>">
		<a href='partner'><span>Partners</span></a>
	</li>
	<li class="<?php if($v_page=='about'){echo 'active ';}?>">
		<a href='about'><span>About Us</span></a>
	</li>
</ul>
</div>
        <!-- ************ END OF CSS MENU *************** -->
	    </nav>
	</div>
</section>
