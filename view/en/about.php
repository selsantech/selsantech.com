<!DOCTYPE html>
<html>
<head>
	<?php include 'common_head.php'; ?>
	<link rel="stylesheet" type="text/css" href="./css/about.css">
</head>
<body>
	<?php include 'before_body.php'; ?>
	
<div class="content blue_background centeredbox">
	<h1>About Us</h1>
	<div class="about-item">
   		<img src="./image/about_us.jpg" />
   		<p><strong>SelsanTech</strong> is a company that provides technology for creating or improving automation processes. The company has a highly qualified team offering custom-made products and services for the industry and the services sector.</p>
		<p>In activity since 2012, <strong>SelsanTech</strong> has expertise in specialized hardware and software development. Its team has already developed projects in the areas of computational intelligence, information security, computational vision and commercial automation.</p>
		<br/>
		<p>Antonio Henrique Pinto Selvatici, PhD
		<br/><strong><a href="mailto:antoniohps@selsantech.com">antoniohps@selsantech.com</a></strong></p>
		<p>Humberto Sandmann, PhD
		<br/><strong><a href="mailto:humberto@selsantech.com">humberto@selsantech.com</a></strong></p>
   	</div>
   	<?php include 'contact.php'; ?>
</div>
	<?php include 'after_body.php'; ?>
</body>
</html>
