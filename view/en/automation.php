<!DOCTYPE html>
<html>
<head>
	<?php include 'common_head.php'; ?>
	<link rel="stylesheet" type="text/css" href="./css/automation.css">
	<script src="./js/sliderpane.js"></script>
</head>
<body>
	<?php include 'before_body.php'; ?>
<div class="content blue_background">
	<h1>Automation, integration e monitoring</h1>
   	<p><strong>Selsantech</strong> has expertise in conectivity for the <strong>Internet of Things (IoT)</strong>, with integration of its own products or third-party devices.</p>
	<div id="automation" class="centeredbox">
		<a href="residential"><div class="info-box residence" onmouseover="slideText('residence')" onmouseout="unslideText('residence')">
			<h3 class="info-title">Home</h3>
    		<div id="residence-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="residence-text" class="hover-text" style="margin: 0px;"> 
    		<ul class=info-text>
    		    <li>Ambient lighting, Temperature control, etc</li>
	    		<li>Scene programming</li>
	    		<li>Reduced construction and cabling</li>
	    		<li>Control using the Internet</li>
   	    	</ul> </div>    	
   		</div></a><a href="building"><div class="info-box building" onmouseover="slideText('building')" onmouseout="unslideText('building')">
			<h3 class="info-title">Building</h3>
    		<div id="building-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="building-text" class="hover-text" style="margin: 0px;"> 
			<ul class="info-text">
				<li>Real time environment monitoring and controlling</li>
				<li>Tracking of devices and products in real time - ZigTrack&reg</li>
				<li>Hotels, hospitals, airports, etc</li>
			</ul></div>
   		</div></a><a href="smartcities"><div class="info-box smartcities" onmouseover="slideText('smartcities')" onmouseout="unslideText('smartcities')">
			<h3 class="info-title">Smart Cities</h3>
       		<div id="smartcities-pane" class="hover-pane" style="margin: 0px;"> </div>
    		<div id="smartcities-text" class="hover-text" style="margin: 0px;"> 
			<ul class="info-text">
				<li>Public lighting, signaling traffic, alert systems, etc</li>
				<li>Remote monitoring and central control</li>
				<li>Audit of public concession</li>
			</ul> </div>
		</div></a>
   	</div>
   	<?php include 'contact.php'; ?>
</div>
	<?php include 'after_body.php'; ?>
</body>
</html>
