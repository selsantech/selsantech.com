<!DOCTYPE html>
<html>
<head>
	<?php include 'common_head.php'; ?>
	<link rel="stylesheet" type="text/css" href="./css/partner.css">
</head>
<body>
	<?php include 'before_body.php'; ?>
<div class="content blue_background">
	<h1>Partners</h1>
	<a href="http://www.fapesp.br" target="_blank"><div class="partner-item"><img src="./image/partner_fapesp.jpg" /></div></a>
	<a href="http://www.systax.com.br" target="_blank"><div class="partner-item"><img src="./image/partner_systax.jpg" /></div></a>
	<a href="http://www.minimo.net.br" target="_blank"><div class="partner-item"><img src="./image/partner_minimo.jpg" /></div></a>
	<a href="http://www.thermotechtt.com.br/" target="_blank"><div class="partner-item"><img src="./image/partner_thermotech.jpg" /></div></a>
	<a href="http://www.alinha.me/" target="_blank"><div class="partner-item"><img src="./image/partner_alinha.jpg" /></div></a>
</div>
	<?php include 'after_body.php'; ?>
</body>
</html>