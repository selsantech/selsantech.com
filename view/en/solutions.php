<!DOCTYPE html>
<html>
<head>
	<?php include 'common_head.php'; ?>
	<link rel="stylesheet" type="text/css" href="./css/solution.css">
</head>
<body>
	<?php include 'before_body.php'; ?>
<div class="content blue_background centeredbox">
	<h1>Products and Solutions</h1>
   	<p>Besides advanced consulting services, Selsantech also develops off-the-shelf products and solutions, based on self-designed hardware and software</p>
	<div class="solution-item" style="margin-top: 20px;">
		<div class="logo">
			<img src="./image/product_zigbee_module.png" />
		</div>
   		<h2>ZigBee Module</h2>
   		<p>The Zigbee module SSTM4 is a low-cost and low-power embedded solution for wireless communication based on the Texas Instruments CC2530F256 SoC, compatible with ZStack software. The module integrates an IEEE 802.15.4 transducer controlled by a 32MHz 8051 microprocessor, combining high processing power with low power consumption. The module board includes also two connected peripherals, so that it can be used as a battery-powered stand-alone device.</p>
   		<p><a href="zigbee"><button>See more</button></a></p>
   	</div>
   	<div class="solution-item">
		<div class="logo">
   			<img src="./image/product_saidafacil.png" />
   		</div>
		<h2>Saida Fácil</h2>
   		<p>A solution developed having in mind the security and agility of children pick-up at school. <strong>Saída Fácil</strong> monitors the proximity of the parent or their delegate to the school, allowing a better planning of the school pick-up logistics and reliefing local traffic.</p>
		<p><a href="http://www.saidafacil.com/" target="_blank"><button>Visit Website</button></a></p>
   	</div>
   	<div class="solution-item">
		<div class="logo">
   			<img src="./image/product_zigtrack.png" />
   		</div>
		<h2>ZigTrack</h2>
   		<p>A network of devices based on the ZigBee and RFID protocol for localization of persons and/or objects in indoor or outdoor environments. This project is funded by Fundação de Amparo à Pesquisa do Estado de São Paulo, <strong>FAPESP</strong>.</p>
		<p><a href="http://www.zigtrack.com/" target="_blank"><button>Visit Website</button></a></p>
	</div>
   	<div class="solution-item">
   		<div class="logo">
   			<div class="fadein">
	   			<img src="./image/product_fromstation.png" />
	    		<img src="./image/product_fromstation_box.png" />
    		</div>
		</div>
		<h2>fromStation</h2>
   		<p>fromStation broadcasts audio from any source to mobile devices in a Wifi network, allowing costumers of bars and other open-to-public places to listen to shows, and sport matches on TV without being disturbed by the surrounding noise. Gather more people in your business with this hi-tech treat!</p>
		<p><a href="http://www.fromstation.com/" target="_blank"><button>Visit Website</button></a></p>
   	</div>
   	<?php include 'contact.php'; ?>
</div>
	<?php include 'after_body.php'; ?>
</body>
</html>
