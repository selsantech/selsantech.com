<?php
/**
 * Essa função garante que todas as classes
 * da pasta lib serão carregadas automaticamente
 */
function __autoload($st_class)
{
	if(file_exists('lib/'.$st_class.'.php'))
		require_once 'lib/'.$st_class.'.php';
}

/**
 * Verifica qual classe controlador (Controller) o usuário deseja chamar
 * e qual método dessa classe (Action) deseja executar
 * Caso o controlador (controller) não seja especificado, o IndexControllers será o padrão
 * Caso o método (Action) não seja especificado, o indexAction será o padrão
 *
 * @package Exemplo simples com MVC
 * @author DigitalDev
 * @version 0.1.1
 **/
class Application
{
	
	public static function currentBase() {
 		$addr  = isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on" ? "https:" : "http:";
 		$addr .= $_SERVER["SERVER_PORT"] != "80" ? $_SERVER["SERVER_PORT"] . "//" : "//";
 		$addr .= $_SERVER["SERVER_NAME"] . "/";
 		// for development
 		if ($_SERVER["SERVER_NAME"] == "127.0.0.1" || $_SERVER["SERVER_NAME"] == "localhost")
 			$addr .= explode("/", $_SERVER["REQUEST_URI"])[1] . "/";
 		return $addr;
	}
	
	function __construct()
	{
		date_default_timezone_set("America/Sao_Paulo");
	}
	
	/**
	 * Usada pra guardar o nome da classe
	 * de controle (Controller) a ser executada
	 * @var string
	 */
	protected $st_controller;

	/**
	 * Usada para guardar o nome do metodo da
	 * classe de controle (Controller) que deverá ser executado
	 * @var string
	 */
	protected $st_action;

	/**
	 * Verifica se os parâmetros de controlador (Controller) e ação (Action) foram
	 * passados via parâmetros "Post" ou "Get" e os carrega tais dados
	 * nos respectivos atributos da classe
	 */
	private function loadRoute()
	{
		/*
		 * Se o controller nao for passado por GET,
		* assume-se como padrão o controller 'IndexController';
		*/
		$this->st_controller = isset($_REQUEST['c']) ?  $_REQUEST['c'] : 'Index';

		/*
		 * Se a action nao for passada por GET,
		* assume-se como padrão a action 'IndexAction';
		*/
		$this->st_action = isset($_REQUEST['m']) ?  $_REQUEST['m'] : 'index';
	}

	/**
	 *
	 * Instancia classe referente ao Controlador (Controller) e executa
	 * método referente e  acao (Action)
	 * @throws Exception
	 */
	public function dispatch()
	{
		$this->loadRoute();

		//verificando se o arquivo de controle existe
		$st_controller_file = 'controller/'.$this->st_controller.'Controller.php';
		if ($this->st_controller == "Auth")
			$st_controller_file = './user/'.$st_controller_file;
		if(file_exists($st_controller_file))
			require_once $st_controller_file;
		else
			throw new Exception('File '.$st_controller_file.' not found');

		//verificando se a classe existe
		$st_class = $this->st_controller.'Controller';
		if (class_exists($st_class))
			$o_class = new $st_class;
		else
			throw new Exception("Class '$st_class' does not exist into file '$st_controller_file'");

		//verificando se o metodo existe
		if ($this->st_action == null)
			$this->st_action = "home";
		$st_method = $this->st_action.'Action';
		if (method_exists($o_class,$st_method))
			$o_class->$st_method();
		else
			throw new Exception("Method '$st_method' does not exist into class '$st_class'");
	}

	/**
	 * Redireciona a chamada http para outra página
	 * @param string $st_uri
	 */
	public static function redirect( $st_uri )
	{
		header("Location: $st_uri");
	}
}
?>