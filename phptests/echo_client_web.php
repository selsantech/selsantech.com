<html>
 <head>
  <title>PHP Echo Client at Unix Socket</title>
  </head>
 <body>
 <?php 
error_reporting(E_ALL);

/*Set the script time limit - allow it to hang around waiting for connections*/
//set_time_limit(0);

/*Turn on implicit output flushing, so we see what we're getting as it comes in*/
ob_implicit_flush(true);

$filename="/tmp/test_socket_aaa";
$timeout=60;

echo str_repeat(" ", 4096);
echo "<h1> PHP Socket Connection Test: Variables Forwarding </h1> <br/> \r\n";

/*Try to conneect to the socket*/
$fp = stream_socket_client("unix://".$filename, $errno, $errstr, 30);
if (!$fp) {
    echo "$errstr ($errno)<br />\r\n";
} else {

  stream_set_blocking($fp,false);
  
  /* Read greeting messages*/
  usleep(600);
  while(($msg=fgets($fp))!=false) {
  	echo "$msg"."<br/>\r\n";
  	usleep(300);
  }
  
  do {

  	fwrite ($fp, "Testing...\n");
  	/* Read talkback messages*/
  	usleep(500);
  	while(($msg=fgets($fp))!=false) {
  		echo "$msg"."<br/>\r\n";
  		usleep(300);
  	}
  	
  	
    foreach ($_GET as $key => $value ) {
    	fwrite ($fp, "$key: $value\n");
    	
    	/* Read talkback messages*/
    	usleep(500);
    	while(($msg=fgets($fp))!=false) {	
    		echo "$msg"."<br/>\r\n";
    		usleep(300);
    	} 
    }
    
    fwrite ($fp, "quit\n");
     
    /* Read talkback messages*/
    usleep(500);
    while(($msg=fgets($fp))!=false) {
    	echo "$msg"."<br/>\r\n";
    	usleep(300);
    }
        
  } while(false);

  fclose($fp);
}
    
 ?> 
 </body>
</html>

