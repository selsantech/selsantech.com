 <?php 

 require 'http_tunneling_vars.php';

 //error_reporting(E_ALL);
 error_reporting(0);
 /*Set the script time limit - allow it to hang around waiting for connections*/
 //set_time_limit(0);

 /*Turn on implicit output flushing, so we see what we're getting as it comes in*/
 ob_implicit_flush(true);
 while(@ob_end_flush()); //Flush (send) the output buffer and turn off output buffering

 //echo "<h1> PHP Socket Connection Test: Variables Forwarding </h1> <br/> \r\n";

 /*Try to conneect to the socket*/
 $fp = stream_socket_client("unix://".$filename, $errno, $errstr, 30);
 if (!$fp) {
 	//TODO: return an error message readable by the application
 	echo "$debug_start$errstr ($errno)$debug_end<br />\r\n";
 } else {

  stream_set_blocking($fp,false);

  /* Read greeting messages*/
  usleep(600);
  while(($msg=fgets($fp))!=false) {
  	//echo "$msg";
  	usleep(300);
  }

  do {
  	 
  	fwrite($fp,"<$vars_tag_name>");
  	foreach ($_GET as $key => $value ) {
  		fwrite ($fp, make_httpvar_string($key, $value));
  	}
  	foreach ($_POST as $key => $value ) {
  		fwrite ($fp, make_httpvar_string($key, $value));
  	}
  	fwrite($fp,"</$vars_tag_name>\r\n");

  	/* Read talkback messages*/
  	usleep(500);
  	while(($msg=fgets($fp))!=false) {
  		//echo "$msg"."\r\n";
  		usleep(300);
  	}


  	fwrite ($fp, "$quit_string\n");
  	 
  	/* Read talkback messages*/
  	usleep(500);
  	while(($msg=fgets($fp))!=false) {
  		//echo "$msg"."\r\n";
  		usleep(300);
  	}

  } while(false);

  fclose($fp);
 }

 ?>
