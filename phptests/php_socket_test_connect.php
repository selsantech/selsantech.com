<html>
 <head>
  <title>PHP Socket Connection Test</title>
  </head>
 <body>
 <?php 
error_reporting(E_ALL);

/*Set the script time limit - allow it to hang around waiting for connections*/
set_time_limit(0);

/*Turn on implicit output flushing, so we see what we're getting as it comes in*/
ob_implicit_flush();

$address='localhost';
$port=10001;
$filename="/tmp/test_socket_abcdefgh";
$timeout=60;

echo str_repeat(" ", 4096);
echo "<h1> PHP Socket Connection Test </h1> <br/> \r\n";

/*Try to conneect to the socket*/
$fp = stream_socket_client("unix://".$filename, $errno, $errstr, 30);
if (!$fp) {
    echo "$errstr ($errno)<br />\r\n";
} else {

  stream_set_blocking($fp,false);
  do {

    /* Read incoming messages*/
    usleep(300);
    while(($msg=fgets($fp))!=false) {
      echo "$msg"."<br/>\r\n";
    }
    
    //while(!(($inp = fgetc($stdin))===false) || $inp=='\n') {
    //  fwrite ($fp, $inp, 1);
    //  echo $inp;  
    //}
    $inp=fread(STDIN,1024);
    fwrite ($fp, $inp);
    echo "Echoing: $inp\r\n";
    flush();
  
  } while(true);
  fclose($fp);
}
    
 ?> 
 </body>
</html>


