<html>
 <head>
  <title>PHP Downloading mode test</title>
 </head>
 <body>
 <?php

$i=0;
// All you need is 4096 spaces first
echo str_repeat(" ", 4096);
//ob_implicit_flush(true);
while (@ob_end_flush()); //Flush (send) the output buffer and turn off output buffering

while($i<10) {
 
  // and ANY TAG before \r\n
  echo "<p>Hello $i</p><br/>\r\n"; $i++;
  @flush();
  sleep(5);
}

 ?>
 </body>
</html>