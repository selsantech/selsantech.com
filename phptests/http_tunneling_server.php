 <?php 

/*----------- Constants ------------------*/
require 'http_tunneling_vars.php';
/*----------------------------------------*/

error_reporting(E_ALL);

/*Set the script time limit - allow it to hang around waiting for connections*/
set_time_limit(0);

/*Turn on implicit output flushing, so we see what we're getting as it comes in*/
ob_implicit_flush(true);
while(@ob_end_flush()); //Flush (send) the output buffer and turn off output buffering

echo "$debug_start PHP HTTPRequest Variables Tunneling Server <br/>$debug_end\r\n";

/*Try to create the socket*/
if(($sock=socket_create(AF_UNIX,SOCK_STREAM,0))==false) {
  echo "$debug_start socket_create() failed. Reason: ".socket_strerror(socket_last_error())."<br/>$debug_end \r\n";
} else echo "$debug_start Socket created <br\>$debug_end \r\n";

/*Try to bind the socket to the port*/
//To check whether another process is already listening, try to connect to the socket first
@unlink($filename); //The @ removes warning messages
if(socket_bind($sock,$filename)==false) {
  echo "$debug_start socket_bind() failed, address = $filename. Reason: ".socket_strerror(socket_last_error($sock))."<br/>$debug_end \r\n";
} else echo "$debug_start Binding socket<br>$debug_end \r\n";

/*Try to listen to the port*/
if(socket_listen($sock,$max_waiting_connections)==false) {
  echo "$debug_start socket_listen() failed. Reason: ".socket_strerror(socket_last_error($sock))."<br/>$debug_end \r\n";
} else echo "$debug_start Listening to socket<br>$debug_end \r\n";

//TODO: Allow for exiting if no connections after time-out
//$prevTime = time();
$childrenCount = 0;
do {

  /*Wait for an incoming connection*/
  if(($msg_sock=socket_accept($sock))==false) {
      echo "$debug_start socket_accept() failed. Reason: ".socket_strerror(socket_last_error($sock))."<br/>$debug_end \r\n";
      break;
  }
  
  /*Now the process forks. The parent keeps waiting for new connections and 
   * for children to die. Children make the dirty work and then close.*/
  
  switch($pid=pcntl_fork()) {
  	case -1: //error
  		echo "$debug_start fork() failed. Reason: ".pcntl_strerror(pcntl_get_last_error())."<br/>$debug_end \r\n";
  		break 2;

  	case 0: //child process - send process info and wait for message
  		/*Send info*/
  		$msg = "$debug_start  Welcome to the PHP HTTPRequest Tunneling Server - Child PID: ".posix_getpid().".$debug_end \r\n";
  		socket_write($msg_sock,$msg,strlen($msg));
  		echo "$debug_start Connection OPEN<br/>$debug_end \r\n";
  		
  		//TODO: Implement awaiting timeout
  		do {
  		
  			if(false === ($buf=socket_read($msg_sock,2048,PHP_NORMAL_READ))) {
  				echo "$debug_start socket_read() failed. Reason: ".socket_strerror(socket_last_error($sock))."<br/>$debug_end \r\n";
  				break;
  			}
  		
  			if(!$buf=trim($buf)) {
  				continue;
  			}
  		
  			$talkback = "$ack_string\r\n";
  			socket_write($msg_sock,$talkback,strlen($talkback));
  			
  			if($buf==$quit_string) {
  				echo "$debug_start Disconnecting from socket...<br/>$debug_end \r\n";
  				break;
  			}
  		
  			//Just tunnels the incoming messages 
  			echo "$buf\r\n";
  		
  		} while(true);
  		socket_close($msg_sock);
  		exit(1);
  		break;

  	default:
  		echo "$debug_start Giving birth to PID $pid... <br/>$debug_end \r\n";
  		$childrenCount++;
  		//The parent tries to burry dead children and keep awaiting connections
  		while(($cpid = pcntl_waitpid(-1, $status, WNOHANG)) > 0) {
  			$childrenCount--; //Burried dead child...
  			echo "$debug_start Child PID $cpid was burried...<br/>$debug_end \r\n";
  		}
  		break;
  }
  //If you're here, you must be the parent
}while(true);
//Wait for all children to die
while($childrenCount > 0) {
	if(($cpid = pcntl_waitpid(-1, $status)) > 0) {
		$childrenCount--; //Burried dead child...
		echo "$debug_start Child PID $cpid was burried...<br/>$debug_end \r\n";
	}
}
socket_close($sock);
unlink($filename);

 ?> 


