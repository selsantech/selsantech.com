<html>
 <head>
  <title>PHP Echo Server From Unix Socket</title>
  </head>
 <body>
 <?php 
error_reporting(E_ALL);

/*Set the script time limit - allow it to hang around waiting for connections*/
set_time_limit(0);

/*Turn on implicit output flushing, so we see what we're getting as it comes in*/
ob_implicit_flush(true);
while(@ob_end_flush()); //Flush (send) the output buffer and turn off output buffering

$filename="/tmp/test_socket_aaa";
$timeout=60;
$max_waiting_connections = 5;

echo str_repeat(" ", 4096);
echo "<h1> PHP Echo Server From Unix Socket </h1> <br/> \r\n";

/*Try to create the socket*/
if(($sock=socket_create(AF_UNIX,SOCK_STREAM,0))==false) {
  echo 'socket_create() failed. Reason: '.socket_strerror(socket_last_error())."<br/>\r\n";
} else echo "Socket created<br>\r\n";
//flush();

/*Try to bind the socket to the port*/
//if(socket_bind($sock,$address,$port)==false) {
@unlink($filename); //The @ removes warning messages
if(socket_bind($sock,$filename)==false) {
  echo "socket_bind() failed, address = $filename. Reason: ".socket_strerror(socket_last_error($sock))."<br/>\r\n";
} else echo "Binding socket<br>\r\n";
//flush();

/*Try to listen to the port*/
if(socket_listen($sock,$max_waiting_connections)==false) {
  echo 'socket_listen() failed. Reason: '.socket_strerror(socket_last_error($sock))."<br/>\r\n";
} else echo "Listening to socket<br>\r\n";
//flush();

//TODO: Allow for exiting if no connections after time-out
$prevTime = time();
$childrenCount = 0;
do {

  /*Wait for an incoming connection*/
  if(($msg_sock=socket_accept($sock))==false) {
      echo 'socket_accept() failed. Reason: '.socket_strerror(socket_last_error($sock))."<br/>\r\n";
      break;
  }
  
  /*Now the process forks. The parent keeps waiting for new connections and 
   * for children to die. Children make the dirty work and then close.*/
  
  switch($pid=pcntl_fork()) {
  	case -1: //error
  		echo 'fork() failed. Reason: '.pcntl_strerror(pcntl_get_last_error())."<br/>\r\n";
  		break 2;

  	case 0: //child process - send instruction and wait for message
  		/*Send instructions*/
  		$msg = "\nWelcome to the PHP Test Server - Child PID: ".posix_getpid().". \n".
  				"To quit type 'quit'.\n";
  		socket_write($msg_sock,$msg,strlen($msg));
  		echo "Connection OPEN<br/>\r\n";
  		//flush();
  		
  		//TODO: Implement awaiting timeout
  		do {
  		
  			if(false === ($buf=socket_read($msg_sock,2048,PHP_NORMAL_READ))) {
  				echo 'socket_read() failed. Reason: '.socket_strerror(socket_last_error($sock))."<br/>\r\n";
  				//flush();
  				break;
  			}
  		
  			if(!$buf=trim($buf)) {
  				continue;
  			}
  		
  			if($buf=='quit') {
  				echo "Disconnecting...<br/>\r\n"; flush();
  				break;
  			}
  		
  			$talkback = "PHP: You said '$buf'.\n";
  			socket_write($msg_sock,$talkback,strlen($talkback));
  			echo "$buf <br/> \r\n"; //flush();
  		
  		} while(true);
  		socket_close($msg_sock);
  		exit(1);
  		break;

  	default:
  		//The parent looks for dead children and keep awaiting connections
  		echo "Giving birth to PID $pid... <br/>\r\n";
  		$childrenCount++;
  		while(($cpid = pcntl_waitpid(-1, $status, WNOHANG)) > 0) {
  			$childrenCount--; //Burried dead child...
  			echo "Child PID $cpid was burried...<br/>\r\n";
  		}
  		break;
  }
  //If you're here, you must be the parent
}while(true);
//Wait for all children to die
while($childrenCount > 0) {
	if(($cpid = pcntl_waitpid(-1, $status)) > 0) {
		$childrenCount--; //Burried dead child...
		echo "Child PID $cpid was burried...<br/>\r\n";
	}
}
socket_close($sock);
unlink($filename);

 ?> 
 </body>
</html>


