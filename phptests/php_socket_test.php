<html>
 <head>
  <title>PHP Socket Test</title>
  </head>
 <body>
 <?php 
error_reporting(E_ALL);

/*Set the script time limit - allow it to hang around waiting for connections*/
set_time_limit(0);

/*Turn on implicit output flushing, so we see what we're getting as it comes in*/
ob_implicit_flush(1);
ob_end_flush();

$address='localhost';
$port=10001;
$filename="/tmp/test_socket_abcdefgh";
$timeout=60;

echo str_repeat(" ", 4096);
echo "<h1> PHP Socket Test </h1> <br/> \r\n";

/*Try to create the socket*/
//if(($sock=socket_create(AF_INET,SOCK_STREAM,SOL_TCP))==false) {
if(($sock=socket_create(AF_UNIX,SOCK_STREAM,0))==false) {
  echo 'socket_create() failed. Reason: '.socket_strerror(socket_last_error())."<br/>\r\n";
} else echo "Socket created<br>\r\n";
flush();

/*Try to bind the socket to the port*/
//if(socket_bind($sock,$address,$port)==false) {
if(socket_bind($sock,$filename)==false) {
//if(socket_connect($sock,$filename)==false) {
  echo "socket_bind() failed, address = $filename. Reason: ".socket_strerror(socket_last_error($sock))."<br/>\r\n";
} else echo "Binding socket<br>\r\n";
flush();

/*Try to listen to the port*/
if(socket_listen($sock,5)==false) {
  echo 'socket_listen() failed. Reason: '.socket_strerror(socket_last_error($sock))."<br/>\r\n";
} else echo "Listening to socket<br>\r\n";
flush();

$prevTime = time();
do {

  /*Wait for an incoming connection*/
  if(($msg_sock=socket_accept($sock))==false) {
      echo 'socket_accept() failed. Reason: '.socket_strerror(socket_last_error($sock))."<br/>\r\n";
      break;
  }

  /*Send instructions*/
  $msg = "\nWelcome to the PHP Test Server. \n".
          "To quit type 'quit'.\n";
  socket_write($msg_sock,$msg,strlen($msg));

  echo "Connection OPEN<br/>\r\n";
  flush();

  do {

    if(false === ($buf=socket_read($msg_sock,2048,PHP_NORMAL_READ))) {
      echo 'socket_read() failed. Reason: '.socket_strerror(socket_last_error($sock))."<br/>\r\n";
      flush();
      break 2;
    }

    if(!$buf=trim($buf)) {
      continue;
    }

    if($buf=='quit') {
      echo "Disconnecting...<br/>\r\n"; flush();
      break;
    }

    $talkback = "PHP: You said '$buf'.\n";
    socket_write($msg_sock,$talkback,strlen($talkback));
    echo "$buf <br/> \r\n"; flush();

  } while(true);
  socket_close($msg_sock);

}while(true);
socket_close($sock);
unlink($filename);

 ?> 
 </body>
</html>


